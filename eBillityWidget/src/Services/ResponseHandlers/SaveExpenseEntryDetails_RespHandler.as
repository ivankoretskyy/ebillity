package Services.ResponseHandlers
{
	import Controller.ApplicationManager.ApplicationManager;
	import Controller.ViewsManager.ViewsManager;
	
	import Views.ExpenseEntryScreen;
	import Views.PopUp;
	import Views.RecentEntries;
	
	import flash.data.SQLConnection;
	import flash.data.SQLMode;
	import flash.data.SQLStatement;
	import flash.display.DisplayObject;
	import flash.events.SQLEvent;
	import flash.filesystem.File;
	
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;
	
	import utility.Constants;

	public class SaveExpenseEntryDetails_RespHandler
	{
		//************** For Custom Alert Box Declaration ****************
		protected var alert:PopUp = new PopUp();
		protected var viewsManRef:ViewsManager = ViewsManager.getViewsManagerInstance();
		protected var alertParent:DisplayObject;
		//************** End of Custom Alert Box Declaration ****************
		
		//////		Database Connection and Manipulation	//////////////////
		private var sqlConn : SQLConnection = new SQLConnection();
		private var sqlStm : SQLStatement = new SQLStatement();
		private var dbFile : File ;
		//////////////////////////////////////////////////////////////////////
		
		// This will have the complete response	//////////////////
		private var responseXML:XML = new XML();
		//////////////////////////////////////////////////////////
		
		//**********	Application Manager Reference ************************
		private var appManRef:ApplicationManager = ApplicationManager.getApplicationManagerInstance();
		/////////////////////////////////////////////////////////////////
		
		
		private var _isSynch:String = "";
		private var _entryId:String = "";
		
		public function SaveExpenseEntryDetails_RespHandler()
		{
			//************		Connecting to Database ****************				
			dbFile = File.applicationStorageDirectory.resolvePath(Constants.DATABASE_NAME);
			
			sqlConn.addEventListener(SQLEvent.OPEN, dbOpenResultHandler);
			sqlConn.open(dbFile, SQLMode.CREATE);
			///////////////////////////////////////////////////////
		}
		
		/////////	Database Open Result Handler ////////////////////
		public function dbOpenResultHandler(event:SQLEvent):void
		{
			sqlStm.sqlConnection = sqlConn;
		}
		/////////////////////////////////////////////////////////////
		
		/////////////		Getters and Setters	/////////////////////////////////
		public function get entryId():String
		{
			return _entryId;
		}

		public function set entryId(value:String):void
		{
			_entryId = value;
		}

		public function get isSynch():String
		{
			return _isSynch;
		}

		public function set isSynch(value:String):void
		{
			_isSynch = value;
		}
		/////////////////////////////////////////////////////////////////////
		
		

		public function saveExpenseEntry_ResultHandler(event:ResultEvent):void
		{
			responseXML = XML(event.message.body);
			var newEntryId:String = responseXML.*::Body.*::SaveExpenseEntryDetailsResponse.*::SaveExpenseEntryDetailsResult.*::NewIdentifier;
			
			// If a new entry is created then user comes to recent entries page and a synch call is sent as well.
			// Recent Entry page (Array Collection of DataGrid) will have EPOC number for that entry but the DB would
			// have been updated. So when user opens that entry page then EPOC number is transfered from Array Collection
			// object to entry page and page tries to load that entry (of EPOC number) from DB but the DB would have been 
			// updated by Synch function. So entry page keeps on loading for Infinite Time. So we need to update Array Collection of 
			// RecentEntry Page as well as "loadEntryId" variable of the entry page.
			// Again remember, this is all due to a new entry created.
			if( isSynch == "2" )
			{
				var recentEntriesPageRef:RecentEntries = RecentEntries(viewsManRef.getViewReference(Constants.VIEW_TYPE_RECENTENTRIES));
				
				if( recentEntriesPageRef != null )
				{
					for each ( var arrColObj:Object in recentEntriesPageRef.RecentEntriesArrCol )
					{
						if( arrColObj.entryId == entryId )
						{
							arrColObj.entryId = newEntryId;
						}
					}
				}
				
				var expenseEntryPageRef:ExpenseEntryScreen = ExpenseEntryScreen( viewsManRef.getViewReference(Constants.VIEW_TYPE_EXPENSEENTRY) );
				
				if( expenseEntryPageRef != null && expenseEntryPageRef.loadEntryId==entryId )
				{
					expenseEntryPageRef.loadEntryId = newEntryId;
				}
			}
			////////////////////////////////////////////////////////////////////////////////////////////////////////			

			
			
			if(isSynch != "")// An Existing entry is being updated because new entry also has some (EPOC) ID already
			{
				sqlStm.text = "UPDATE entries SET is_synch='1', entry_id='" + newEntryId + "' WHERE entry_id='" + entryId + 
					"' AND entry_type='EE' AND user_logged_id='" + appManRef.userId_LoggedWith + 
					"' AND firm_logged_id='" + appManRef.firmId_LoggedWith + "'";
				
				sqlStm.execute();
			}
			
			appManRef.entriesToSynchUp = appManRef.entriesToSynchUp - 1;
			
			if( appManRef.entriesToSynchUp == 0 )
			{
				appManRef.isSynchInProgress = false;
				appManRef.isSynchronized = true;
				appManRef.isUnsynchToSynching = false;
			}
			
			
		}
		
		public function saveExpenseEntry_FaultHandler(event:FaultEvent):void
		{
			var xml:XML = XML(event.message.body);
		}
	}
}