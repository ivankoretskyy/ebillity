package Services.ResponseHandlers
{
	import Controller.ApplicationManager.ApplicationManager;
	import Controller.ViewsManager.ViewsManager;
	
	import Views.PopUp;
	import Views.RecentEntries;
	
	import flash.data.SQLConnection;
	import flash.data.SQLMode;
	import flash.data.SQLResult;
	import flash.data.SQLStatement;
	import flash.display.DisplayObject;
	import flash.events.SQLEvent;
	import flash.filesystem.File;
	
	import mx.managers.PopUpManager;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;
	
	import utility.Constants;
	import utility.UserInfo;

	public class LoadExpenseEntry_RespHandler
	{
		//	Application Manager Reference	//////////////////////////////////
		private var appManRef:ApplicationManager = ApplicationManager.getApplicationManagerInstance();
		//////////////////////////////////////////////////////////////////////
		
		//************** For Custom Alert Box Declaration ****************
		protected var alert:PopUp = new PopUp();
		protected var viewsManRef:ViewsManager = ViewsManager.getViewsManagerInstance();
		protected var alertParent:DisplayObject;
		//************** End of Custom Alert Box Declaration ****************
		
		
		///////////		Database Handling variables	////////////////
		private var sqlConn : SQLConnection = new SQLConnection;
		private var sqlStm : SQLStatement = new SQLStatement;
		private var dbFile : File;
		/////////////////////////////////////////////////
		
		
		// This will have the complete response	//////////////////
		private var responseXML:XML = new XML();
		//////////////////////////////////////////////////////////
		
		
		public function LoadExpenseEntry_RespHandler()
		{
			//************		Connecting to Database ****************				
			dbFile = File.applicationStorageDirectory.resolvePath(Constants.DATABASE_NAME);
			
			sqlConn.addEventListener(SQLEvent.OPEN, dbOpenResultHandler);
			sqlConn.open(dbFile, SQLMode.CREATE);
			///////////////////////////////////////////////////////
			
		}
		
		/////////	Database Open Result Handler ////////////////////
		public function dbOpenResultHandler(event:SQLEvent):void
		{
			sqlStm.sqlConnection = sqlConn;
		}
		/////////////////////////////////////////////////////////////
		
		
		
		public function loadExpenseEntry_ResultHandler(event:ResultEvent):void
		{
			var recentEntriesPageRef:RecentEntries = RecentEntries(viewsManRef.getViewReference(Constants.VIEW_TYPE_RECENTENTRIES));;
			
			responseXML = XML(event.message.body);
			
			var xml:XML = responseXML.*::Body.*::LoadExpenseEntryResponse.*::LoadExpenseEntryResult[0];
			
			
			if ( isEntrySynchronized( String(xml.*::ExpenseEntryId), "EE" ) )
			{
				// As entry is synchronized with DB so Update it 
				updateExpenseEntryToDB();
			}
			
			//Now do the following if all Recent Entries has been updated from net to DB:
			// enableComponent = true;
			// call the function PopulateRecentEntryPageWithDB();
			recentEntriesPageRef.recentEntriesCount = recentEntriesPageRef.recentEntriesCount - 1;
			if( recentEntriesPageRef.recentEntriesCount == 0)
			{
				recentEntriesPageRef.enableComponent = true;
				recentEntriesPageRef.flushRecentEntriesFewVariables();
				recentEntriesPageRef.populateRecentEntriesPageFromDB();
			}
			
			
			
		}	
		//////////////////////////////////////////////////////////////////////////////////////////	
		
		//////////////		Fault Handler	///////////////////////////////////////////
		public function loadExpenseEntry_FaultHandler(event:FaultEvent):void
		{
			//Enable DateField component and Right Left Buttons
			var recentEntriesPageRef:RecentEntries = RecentEntries(viewsManRef.getViewReference(Constants.VIEW_TYPE_RECENTENTRIES));;
			recentEntriesPageRef.enableComponent = true;
			
			//*******Custom Alert Box Code*****************				
			alert.message = Constants.ERROR_MSG_NO_CONNECTION; 				
			alert.titlestr = Constants.ERROR_TITLE_LOADEXPENSEENTRY_FAULT;
			alertParent = DisplayObject( viewsManRef.getViewReference(Constants.VIEW_TYPE_RECENTENTRIES) );
			
			PopUpManager.addPopUp(alert, alertParent, true);
			PopUpManager.centerPopUp(alert);
			//******* End of Custom Alert Box Code*****************
		}
		//////////////////////////////////////////////////////////////////////////////
		
		
		
		//********************		Utility Functions	********************
		//******************************************************************
		
		private function isEntrySynchronized(entryId:String , entryType:String):Boolean
		{
			sqlStm.text = "SELECT entry_id, entry_type FROM entries WHERE entry_id='" + entryId + 
				"' and entry_type='" + entryType + "' and is_synch='1'";
			sqlStm.execute();
			
			var result:SQLResult = sqlStm.getResult();
			
			if( result.data == null )
			{
				return false;
			}
			else
			{
				return true;
			}
			
		}
		
		public function updateExpenseEntryToDB():void
		{
			var xml:XML = responseXML.*::Body.*::LoadExpenseEntryResponse.*::LoadExpenseEntryResult[0];
			
			//	Setting entryDate for DB insertion format from YYYY-MM-DDT00:00 to YYYY-MM-DD
			var entryDate:String = xml.*::ExpenseEntryDate;
			var entryDateArr:Array = entryDate.split("T");
			entryDate = entryDateArr[0];
			///////////////////////////////////////////////////
			
			
			// Setting ExpenseTypeName for DB insertion
			var expenseTypeName:String = doubleApostropheIfExists( xml.*::ExpenseTypeName);
			
			// Setting UserName for DB insertion
			var userName:String = doubleApostropheIfExists( xml.*::UserName );
			
			// Setting clientName for DB insertion
			var clientName:String = doubleApostropheIfExists( xml.*::ClientName );
			
			// Setting ProjectName for DB insertion
			var projectName:String = doubleApostropheIfExists( xml.*::ProjectName);
			
			// Setting Descripntion for DB insertion
			var description:String = doubleApostropheIfExists( xml.*::Description );
			
			
			
			sqlStm.text = "UPDATE entries SET " + 
				"user_logged_id='" + appManRef.userId_LoggedWith + "', " +
				"firm_logged_id='" + appManRef.firmId_LoggedWith + "', " + 		
				"entry_date='" + entryDate + "', " + 
				"client_id='" + xml.*::ClientId + "', " + 
				"client_name='" + clientName + "', " + 
				"project_id='" + xml.*::ProjectId + "', " + 
				"project_name='" + projectName + "', " + 
				"user_id='" + xml.*::UserId + "', " + 
				"user_name='" + userName + "', " + 
				"firm_id='" + xml.*::FirmId + "', " + 
				"firm_expense_type_id='" + xml.*::FirmExpenseTypeId + "', " + 
				"expense_type_name='" + expenseTypeName + "', " + 
				"cost='" + xml.*::Cost + "', " + 
				"quantity='" + xml.*::Quantity + "', " + 
				"mocup_ratio='" + xml.*::MocupRatio + "', " + 
				"total_cost='" + xml.*::TotalCost + "', " + 
				"is_reimbursed='" + xml.*::IsReimbursed + "', " + 
				"is_taxable='" + xml.*::IsTaxable + "', " + 
				"tax='" + xml.*::Tax + "', " + 
				"description='" + description + "', " + 
				"is_invoiced='" + xml.*::IsInvoiced + "' " + 
				"WHERE entry_id='" + xml.*::ExpenseEntryId + "' and entry_type='EE'";
			
			
			sqlStm.execute();
			
			
		}
		
		
		
		//	Some strings can have Apostrophe (') so to add such strings into DB we need to double the apostrophe character
		public function doubleApostropheIfExists(nameStr:String):String
		{
			var nameArr:Array = nameStr.split("'");
			nameStr = nameArr.join("''");
			
			return nameStr;
		}
		
	}
}