package Services.ResponseHandlers
{
	import Controller.ApplicationManager.ApplicationManager;
	import Controller.ViewsManager.ViewsManager;
	
	import Services.RequestHandlers.WSReqHandler;
	import Services.ResponseHandlers.LoadExpenseEntry_RespHandler;
	
	import Views.PopUp;
	import Views.RecentEntries;
	
	import flash.data.SQLConnection;
	import flash.data.SQLMode;
	import flash.data.SQLResult;
	import flash.data.SQLStatement;
	import flash.display.DisplayObject;
	import flash.events.SQLEvent;
	import flash.filesystem.File;
	
	import mx.collections.ArrayCollection;
	import mx.managers.PopUpManager;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;
	
	import utility.Constants;
	import utility.DateManipulation;

	public class GetRecentEntriesList_RespHandler
	{
		//	Application Manager Reference
		private var appManRef:ApplicationManager = ApplicationManager.getApplicationManagerInstance();
		
		//	For invoking Web Service
		private var invokeWS:WSReqHandler = new WSReqHandler();
		private var handleExpenseWSResp:LoadExpenseEntry_RespHandler = new LoadExpenseEntry_RespHandler();
		private var handleTimeWSResp:LoadTimeEntry_RespHandler = new LoadTimeEntry_RespHandler();
		
		//************** For Custom Alert Box Declaration ****************
		protected var alert:PopUp = new PopUp();
		protected var viewsManRef:ViewsManager = ViewsManager.getViewsManagerInstance();
		protected var alertParent:DisplayObject;
		//************** End of Custom Alert Box Declaration ****************
		
		///////////		Database Handling variables	////////////////
		private var sqlConn : SQLConnection = new SQLConnection;
		private var sqlStm : SQLStatement = new SQLStatement;
		private var dbFile : File;
		/////////////////////////////////////////////////
		
		// RecentEntries page will set this variable before invoking this API. This date is being used
		// to delete the entries in DB (if exist any) if there is no entry in response of API. 
		public var recentEntriesDate:String = "";
		
		
		private var responseXML:XML = new XML();
		//private var recentEntriesArrCol:ArrayCollection = new ArrayCollection();
		
		
		
		public function GetRecentEntriesList_RespHandler()
		{
			//************		Connecting to Database ****************				
			dbFile = File.applicationStorageDirectory.resolvePath(Constants.DATABASE_NAME);
			
			sqlConn.addEventListener(SQLEvent.OPEN, dbOpenResultHandler);
			sqlConn.open(dbFile, SQLMode.CREATE);
			///////////////////////////////////////////////////////
		
		}
		
		/////////	Database Open Result Handler ////////////////////
		public function dbOpenResultHandler(event:SQLEvent):void
		{
			sqlStm.sqlConnection = sqlConn;
		}
		/////////////////////////////////////////////////////////////
		
		
		
		//***************************** GetRecentEntriesList API material *******************************
		//****************************************************************************** 
		
		////////////////	API Result Handler	///////////
		public function getRecentEntriesList_ResultHandler(event:ResultEvent):void
		{
			var recentEntriesPageRef:RecentEntries = RecentEntries(viewsManRef.getViewReference(Constants.VIEW_TYPE_RECENTENTRIES));;
			
			responseXML = XML(event.message.body);
			
			recentEntriesPageRef.recentEntriesCount = responseXML.*::Body.*::GetRecentEntriesListResponse.*::GetRecentEntriesListResult.*::RecentEntriesCount;
			
			// Deleting the Date entries which are already synched
			// In this way the entries which are deleted on web are also deleted here
			sqlStm.text = "DELETE FROM entries WHERE is_synch='1' and entry_date='" + recentEntriesDate + 
				"' and user_logged_id='" + appManRef.userId_LoggedWith + "' and firm_logged_id='" + appManRef.firmId_LoggedWith + "'";
			sqlStm.execute();
			
			
			if(responseXML.*::Body.*::GetRecentEntriesListResponse.*::GetRecentEntriesListResult.*::RecentEntriesCount == 0)
			{	
				recentEntriesPageRef.populateRecentEntriesPageFromDB();
				recentEntriesPageRef.enableComponent = true;
			}
			else
			{
				var xml:XML;
				
				// These 2 variables to handle entry types other than "EE" and "TE", forexample type "PN"
				var isAnyEEorTEEntry:Boolean = false;
				var entryCount:int = 0;
				var totalEntriesCount:int = responseXML.*::Body.*::GetRecentEntriesListResponse.*::GetRecentEntriesListResult.*::RecentEntriesCount;
				////////////////////////////////////////////////////////////////////
				
				// There is entry for the DateField Value in Response of API call
				// so now call respective API's for each entry
				for each (xml in responseXML.*::Body.*::GetRecentEntriesListResponse.*::GetRecentEntriesListResult.*::RecentEntriesList.*::MobileRecentEntry)
				{
					entryCount++;
					
					//**********	Preparing Parameters for API call
					var param:Object = new Object();
					var context:Object = new Object();
					if (xml.*::RecentType == "EE")
					{
						isAnyEEorTEEntry = true;
						/////////	Storing "is_editable" for this entry /////////////////////
						if( doesEntryExistInDB( String(xml.*::EntryId), String(xml.*::RecentType) ) )
						{
							// Entry does exist in DB
							// Now check if entry is synchronized with internet then delete the entry and insert 
							// if not synchronized then do nothing
							if ( isEntrySynchronized( String(xml.*::EntryId), String(xml.*::RecentType) ) )
							{
								// Deleting the entry
								sqlStm.text = "DELETE FROM entries WHERE entry_id='" + String(xml.*::EntryId) + 
									"' and entry_type='" + String(xml.*::RecentType) + "'";
								sqlStm.execute();
								
								// Inserting the entry
								sqlStm.text = "INSERT INTO entries (entry_id, entry_type, is_synch, is_editable) " +
									"VALUES ('" + String(xml.*::EntryId) + "', '" + String(xml.*::RecentType) + "', '1', '" +
									String(xml.*::IsEditable) + "')";
								sqlStm.execute();
							}
						}
						else
						{
							// Entry does not exist in DB so Create a new entry with "is_editable" and "is_synch = 1" 
							sqlStm.text = "INSERT INTO entries (entry_id, entry_type, is_synch, is_editable) " +
								"VALUES ('" + String(xml.*::EntryId) + "', '" + String(xml.*::RecentType) + "', '1', '" +
								String(xml.*::IsEditable) + "')";
							sqlStm.execute();
						}
						
						/////////	Parameters //////////////////////////////////
						var expenseEntryId:String = "";
						
						context.SessionId = appManRef.sessionId_LoggedWith;
						expenseEntryId = String(xml.*::EntryId);
						
						param.context = context;
						param.expenseEntryId = expenseEntryId;
						//////////////////////////////////////////////////
						
						//////	Call LoadExpenseEntry to get it's Response XML
						invokeWS.invokeWebService(Constants.API_TYPE_LOADEXPENSEENTRY, param, handleExpenseWSResp.loadExpenseEntry_ResultHandler, handleExpenseWSResp.loadExpenseEntry_FaultHandler);
						
						
					}
					else if (xml.*::RecentType == "TE")
					{
						isAnyEEorTEEntry = true;
						/////////	Storing "is_editable" for this entry /////////////////////
						if( doesEntryExistInDB( String(xml.*::EntryId), String(xml.*::RecentType) ) )
						{
							// Entry does exist in DB
							// Now check if entry is synchronized with internet then delete the entry and insert 
							// if not synchronized then do nothing
							if ( isEntrySynchronized( String(xml.*::EntryId), String(xml.*::RecentType) ) )
							{
								// Deleting the entry
								sqlStm.text = "DELETE FROM entries WHERE entry_id='" + String(xml.*::EntryId) + 
									"' and entry_type='" + String(xml.*::RecentType) + "'";
								sqlStm.execute();
								
								// Inserting the entry
								sqlStm.text = "INSERT INTO entries (entry_id, entry_type, is_synch, is_editable) " +
									"VALUES ('" + String(xml.*::EntryId) + "', '" + String(xml.*::RecentType) + "', '1', '" +
									String(xml.*::IsEditable) + "')";
								sqlStm.execute();
							}
						}
						else
						{
							// Entry does not exist in DB so Create a new entry with "is_editable" and "is_synch = 1" 
							sqlStm.text = "INSERT INTO entries (entry_id, entry_type, is_synch, is_editable) " +
								"VALUES ('" + String(xml.*::EntryId) + "', '" + String(xml.*::RecentType) + "', '1', '" +
								String(xml.*::IsEditable) + "')";
							sqlStm.execute();
						}			
						
						
						
						//**********	Preparing Parameters for API call
						var timeEntryArgs:Object = new Object();
						
						context.SessionId = appManRef.sessionId_LoggedWith;
						
						timeEntryArgs.TimeEntryId = String(xml.*::EntryId);
						timeEntryArgs.BillableTypeId = "0";
						
						param.context = context;
						param.timeEntryArgs = timeEntryArgs;
						//////////////////////////////////////////////////
						
						//////	Call LoadExpenseEntry to get it's Response XML
						invokeWS.invokeWebService(Constants.API_TYPE_LOADTIMEENTRY, param, handleTimeWSResp.loadTimeEntry_ResultHandler, handleTimeWSResp.loadTimeEntry_FaultHandler);		
					}
					else
					{
						
						if( isAnyEEorTEEntry == false && entryCount == totalEntriesCount)
						{
							recentEntriesPageRef.enableComponent = true;
							recentEntriesPageRef.flushRecentEntriesFewVariables();
							recentEntriesPageRef.populateRecentEntriesPageFromDB();
						}
						
						recentEntriesPageRef.recentEntriesCount = recentEntriesPageRef.recentEntriesCount - 1;
					}
				}
			}
			
			
		}
			
		private function doesEntryExistInDB(entryId:String , entryType:String):Boolean
		{
			sqlStm.text = "SELECT entry_id, entry_type FROM entries WHERE entry_id='" + entryId + 
				"' and entry_type='" + entryType + "'";
			sqlStm.execute();
			
			var result:SQLResult = sqlStm.getResult();
			
			if( result.data == null )
			{
				return false;
			}
			else
			{
				return true;
			}
			
		}
		
		
		private function isEntrySynchronized(entryId:String , entryType:String):Boolean
		{
			sqlStm.text = "SELECT entry_id, entry_type FROM entries WHERE entry_id='" + entryId + 
				"' and entry_type='" + entryType + "' and is_synch='1'";
			sqlStm.execute();
			
			var result:SQLResult = sqlStm.getResult();
			
			if( result.data == null )
			{
				return false;
			}
			else
			{
				return true;
			}
			
		}
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			/*
				var xml:XML;
				var hour:int = 0;
				var min:int = 0;
				var totalTime:String = "";
				
				for each (xml in responseXML.*::Body.*::GetRecentEntriesListResponse.*::GetRecentEntriesListResult.*::RecentEntriesList.*::MobileRecentEntry)
				{
					var obj:Object = new Object();
					
					obj.clientField = xml.*::Client;
					obj.projectField = xml.*::Project;
					//obj.amountField = ;
				
					
					///////		Changing the Time Format for "Time Entries"
					if (xml.*::RecentType == "TE")
					{
						var timeStr:String = "";
						var timeArr:Array;
						
						timeStr = xml.*::Details;
						
						timeArr = timeStr.split(' ');
						hour = hour + int(timeArr[0]);
						min = min + int(timeArr[2]);
						
						if ( int(timeArr[0]) < 10 )
						{
							timeStr = "0" + timeArr[0] + ".";
						}
						else
						{
							timeStr = timeArr[0] + ".";
						}
						
						if ( int(timeArr[2]) < 10 )
						{
							timeStr = timeStr + "0" + timeArr[2];
						}
						else
						{
							timeStr = timeStr + timeArr[2];
						}
						
						obj.timeField = timeStr;
					}
					else
					{
						obj.timeField = xml.*::Details;
					}
					
					recentEntriesArrCol.addItem(obj);
				}
				/////////	Now the time field values format is set so we can set the Array Collection
				recentEntriesPageRef.RecentEntriesArrCol = recentEntriesArrCol;
				
				///		Now setting the total time for Recent Entry page
						/////	if minutes are greater than 59 then add the quotient to hour
				if (min > 59)
				{
					var temp:int = 0;
					
					temp = min/60;
					
					hour = hour + temp;
					
					min = ( (min/60) - int(min/60) ) * 60; 
				}
				
				
				if(hour < 10)
				{
					totalTime = "0" + String(hour) + ".";
				}
				else
				{
					totalTime = String(hour) + ".";
				}
				
				if(min < 10)
				{
					totalTime = totalTime + "0" + String(min);
				}
				else
				{
					totalTime = totalTime + String(min);
				}
			
				////	Setting the Total Time Bindable variable of RecentEntries page
				recentEntriesPageRef.totalTime = totalTime;
			}
			
			
			
			
			
			
			
			//recentEntriesPageRef.enableComponent = true;
		}
			*/
			
			
			
			
			
		public function getRecentEntriesList_FaultHandler(event:FaultEvent):void
		{
			//Enable DateField component and Right Left Buttons
			var recentEntriesPageRef:RecentEntries = RecentEntries(viewsManRef.getViewReference(Constants.VIEW_TYPE_RECENTENTRIES));;
			recentEntriesPageRef.enableComponent = true;
			
			//*******Custom Alert Box Code*****************				
			alert.message = Constants.ERROR_MSG_NO_CONNECTION; 				
			alert.titlestr = Constants.ERROR_TITLE_GETRECENTENTRIES_FAULT;
			alertParent = DisplayObject( viewsManRef.getViewReference(Constants.VIEW_TYPE_RECENTENTRIES) );
			
			PopUpManager.addPopUp(alert, alertParent, true);
			PopUpManager.centerPopUp(alert);
			//******* End of Custom Alert Box Code*****************
			
		}
	}
}