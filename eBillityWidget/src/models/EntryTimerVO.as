package models
{

	public class EntryTimerVO
	{
		private var _isPaused:Boolean;

		private var _entryId:String;

		private var _hour:String;

		private var _minute:String;

		private var _second:String;

		private var _timerAtStart:int;

		private var _timerAtPause:int;

		public function get timerAtPause():int
		{
			return _timerAtPause;
		}

		public function set timerAtPause(value:int):void
		{
			_timerAtPause = value;
		}

		public function get timerAtStart():int
		{
			return _timerAtStart;
		}

		public function set timerAtStart(value:int):void
		{
			_timerAtStart = value;
		}

		public function get second():String
		{
			return _second;
		}

		public function set second(value:String):void
		{
			_second = value;
		}

		public function get minute():String
		{
			return _minute;
		}

		public function set minute(value:String):void
		{
			_minute = value;
		}

		public function get hour():String
		{
			return _hour;
		}

		public function set hour(value:String):void
		{
			_hour = value;
		}

		public function get entryId():String
		{
			return _entryId;
		}

		public function set entryId(value:String):void
		{
			_entryId = value;
		}

		public function get isPaused():Boolean
		{
			return _isPaused;
		}

		public function set isPaused(value:Boolean):void
		{
			_isPaused = value;
		}

	}
}
