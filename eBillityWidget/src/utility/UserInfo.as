package utility
{
	public class UserInfo
	{
		private var userId:String;
		private var userName:String;
		private var userType:String;
		private var userStatus:String;
		
		public function UserInfo()
		{
		}
		
		//*************** Setters ********************
		public function setUserId(id:String) : void
		{
			userId = id;
		}
		
		public function setUserName(name:String) : void
		{
			userName = name;
		}
		
		public function setUserType(type:String) : void
		{
			userType = type;
		}
		
		public function setUserStatus(status:String) : void
		{
			userStatus = status;
		}
		////////////////// Setters end ///////////////////
		
		//****************** Getters *********************
		public function getUserId() : String
		{
			return userId;
		}
		
		public function getUserName() : String
		{
			return userName;
		}
		
		public function getUserType() : String
		{
			return userType;
		}
		
		public function getUserStatus() : String
		{
			return userStatus;
		}
		////////////// Getters end /////////////////////////////
		
	}
}