package utility {
	
	import flash.desktop.NativeApplication;

	public class Utils {
		
		static public function get applicationId():String {
			var desc:XML = NativeApplication.nativeApplication.applicationDescriptor;
			var ns:Namespace = desc.namespace();
			return desc.ns::id;	
		}
		
		static public function get applicationName():String {
			var desc:XML = NativeApplication.nativeApplication.applicationDescriptor;
			var ns:Namespace = desc.namespace();
			return desc.ns::name;	
		}
	
	}
}