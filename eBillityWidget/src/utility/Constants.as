package utility
{
	public class Constants
	{
		public function Constants()
		{
		}
		
		public static var EBILLITY_VERSION:String = "Time Tracker + Billing for Desktop";
		
		//public static var WEBSERVICE_WSDL:String = "http://www.ebillity.com/WCFService/MobileService.svc/mex?wsdl"; // Production server
		//public static var WEBSERVICE_SERVICE:String = "http://www.ebillity.com/WCFService/MobileService.svc"; // Production server
		
		//public static var WEBSERVICE_WSDL:String = "http://qa.ebillity.com:81/WcfService2/MobileService.svc/mex?wsdl"; // QA
		//public static var WEBSERVICE_SERVICE:String = "http://qa.ebillity.com:81/WcfService2/MobileService.svc"; // QA
		
		//public static var WEBSERVICE_WSDL:String = "http://mobile.ebillity.com/wcfservice/mobileservice.svc?wsdl"; // Mobile
		//public static var WEBSERVICE_SERVICE:String = "http://mobile.ebillity.com/wcfservice/mobileservice.svc"; // Mobile
		
		public static var webserviceUrl:String = "http://mobile.ebillity.com/wcfservice/mobileservice.svc";
		//public static var webserviceUrl:String = "http://tbmobile.ebillity.com/wcfservice/mobileservice.svc​";
		//public static var webserviceUrl:String = "http://tbmobile.ebillity.com/wcfservice/mobileservice.svc";
		public static function get WEBSERVICE_WSDL():String { return webserviceUrl + "/mex?wsdl"; }
		public static function get WEBSERVICE_SERVICE():String { return webserviceUrl; }
		
		public static const NETWORK_DETECTION_POINT:String = WEBSERVICE_SERVICE;
		
		//*****************		Sqlite Database Information ********
		public static const DATABASE_NAME:String = 'timeTracking.db';
		// DB Creation string Version: 5
		public static const DB_TABLE_ENTRIES_CREATE_QUERY:String = "create table if not exists entries (entry_id text, entry_type text, is_synch text, user_logged_id text, firm_logged_id text, is_editable text, entry_date text, client_id text, client_name text, project_id text, project_name text, user_id  text, user_name text, firm_id text, firm_expense_type_id text, expense_type_name text, cost text, quantity text, mocup_ratio text, total_cost text, is_reimbursed text, is_taxable text, tax text, description text, firm_billable_activity_type_id text, billable_activity_type_description text, time_entry_type text, billable_type_id text, billable_type text, from_hour text, from_minute text, to_hour text, to_minute text, flat_fee_amount text, award_amount text, contingency_percentage text, contingency_amount text, is_override_total_time text, override_total_hour text, override_total_minute text, rate text, is_override_rate text, override_rate text, is_billable text, exclude_from_invoice text, use_prepaid_hours text, elapsed_hour text, elapsed_minute text, labor_hour text, labor_minute text, travel_hour text, travel_minute text, total_hour text, total_minute text, is_auto_generated text, invoice_description text, internal_description text, override_total_time_comments text, is_invoiced text, rate_source text, PRIMARY KEY (entry_id, entry_type))" ;
		public static const DB_TABLE_USER_CREDENTIAL_CREATE_QUERY:String = "create table if not exists user_credential (id integer primary key AUTOINCREMENT, user_name text, password text, user_id text)";
		public static const DB_TABLE_USER_FIRM_LIST_CREATE_QUERY:String = "create table if not exists user_firm_list (user_id text, firm_id text, firm_name text, firm_status text, user_type text, user_status text, user_name text, PRIMARY KEY (user_id, firm_id))";
		public static const DB_TABLE_DO_LOGIN_FIRM_CREATE_QUERY:String = "create table if not exists do_login_firm (user_id text, firm_id text, token text, currency_name text, user_label text, client_label text, project_label text, labor_label text, is_financial_info_set text,  PRIMARY KEY (user_id, firm_id))";
		public static const DB_TABLE_GET_EXPENSE_TYPE_CREATE_QUERY:String = "create table if not exists get_expense_type_list (user_id text, firm_id text, expense_type_id text, expense_type_description text, expense_type_amount text, is_expense_type_reimbersable text, is_expense_type_taxable text, is_expense_type_default text, expense_type_tax_percentage text, PRIMARY KEY (user_id, firm_id, expense_type_id))";
		public static const DB_TABLE_USER_LITE_LIST_CREATE_QUERY:String = "create table if not exists user_lite_list (user_logged_id text, firm_id text, user_id text, user_response_name text, PRIMARY KEY (user_logged_id, firm_id, user_id))";
		public static const DB_TABLE_GET_CLIENT_LIST_CREATE_QUERY:String = "create table if not exists get_client_list (user_id text, firm_id text, client_id text, custom_client_id text, client_name text, PRIMARY KEY (user_id, firm_id, client_id))";
		public static const DB_TABLE_GET_PROJECT_LIST_CREATE_QUERY:String = "create table if not exists get_project_list (user_id text, firm_id text, client_id text, project_id text, custom_project_id text, project_name text, PRIMARY KEY (user_id, firm_id, client_id, project_id))";
		public static const DB_TABLE_GET_BILLABLE_ACTIVITIES:String = "create table if not exists get_billable_activities (user_id text, firm_id text, billable_activity_id text, activity_type text, rate_apply_as text, billable_rate text, is_default text, PRIMARY KEY (user_id, firm_id, billable_activity_id))";
		public static const DB_TABLE_GET_RATE_AND_SOURCE:String = "create table if not exists get_rate_and_source_details (user_id text, firm_id text, client_id text, project_id text, firm_billable_activity_type_id text, rate text, rate_source text, overtime_rate text, PRIMARY KEY (user_id, firm_id, client_id, project_id, firm_billable_activity_type_id))";
		public static const DB_TABLE_SETTING:String = "create table if not exists setting (user_id text, firm_id text, auto_start_timer text, is_rounding text, rounding_hour text, rounding_minute text, PRIMARY KEY (user_id, firm_id))";
		public static const DB_TABLE_GET_PROJECT_DETAILS:String = "create table if not exists get_project_details (user_id text, firm_id text, project_id text, billable_type_id text, contingency_percentage text, PRIMARY KEY (user_id, firm_id, project_id))";
		
		//******************* Screen Constants ****************
		public static const VIEW_TYPE_LOGIN:String = "LoginScreen";
		public static const VIEW_TYPE_FIRMLIST:String = "FirmList";
		public static const VIEW_TYPE_RECENTENTRIES:String = "RecentEntries";
		public static const VIEW_TYPE_EXPENSEENTRY:String = "ExpenseEntryScreen";
		public static const VIEW_TYPE_TIMEENTRY:String = "TimeEntryScreen";
		public static const VIEW_TYPE_SETTING:String = "SettingScreen";
		//////////////////////////////////////////////////////////
		
		//****************** API Call Constants ******************
		public static const API_TYPE_VALIDATEUSER:String = "ValidateUser";
		public static const API_TYPE_DOFULLLOGIN:String = "DoLoginFirm";
		public static const API_TYPE_GETRECENTENTRIESLIST:String = "GetRecentEntriesList";
		public static const API_TYPE_LOADEXPENSEENTRY:String = "LoadExpenseEntry";
		public static const API_TYPE_LOADTIMEENTRY:String = "LoadTimeEntry";
		public static const API_TYPE_GET_EXPENSE_TYPE:String = "GetExpenseTypeList";
		public static const API_TYPE_USER_LITE_LIST:String = "UserLiteList";
		public static const API_TYPE_GET_CLIENT_LIST:String = "GetClientList";
		public static const API_TYPE_GET_PROJECT_LIST:String = "GetProjectList";
		public static const API_TYPE_SAVE_EXPENSE_ENTRY_DETAILS:String = "SaveExpenseEntryDetails";
		public static const API_TYPE_SAVE_TIME_ENTRY_DETAILS:String = "SaveTimeEntryDetails";
		public static const API_TYPE_GET_BILLABLE_ACTIVITIES:String = "GetBillableActivities";
		public static const API_TYPE_GET_RATE_AND_SOURCE_DETAILS:String = "GetRateAndSourcceDetails";
		public static const API_TYPE_GET_PROJECT_DETAILS:String = "GetProjectDetails";
		///////////////////////////////////////////////////////////
		
		//****************** Error Messages *********************
		//public static const ERROR_MSG_DOFULLLOGIN_FAULT:String = "Fault occurred.";
		public static const ERROR_TITLE_ERROR:String = "ERROR:";
		public static const ERROR_TITLE_DOFULLLOGIN_FAULT:String = "DoFullLogin Fault:";
		public static const ERROR_TITLE_GETRECENTENTRIES_FAULT:String = "GetRecentEntriesLis Fault:";
		public static const ERROR_TITLE_INVALID_DETAILS:String = "Error: Invalid Details";
		public static const ERROR_TITLE_VALIDATION_FAULT:String = "User Validation Fault:";
		public static const ERROR_TITLE_DATABASE_EXCEPTION:String = "DB Exception:";
		public static const ERROR_TITLE_NO_CONNECTION:String = "Unavailable Connection:"; 
		public static const ERROR_TITLE_INVALID_INPUT_FORMAT:String = "Error: Incomplete information";
		public static const ERROR_TITLE_SESSION_EXPIRED:String = "Invalid Session:";
		public static const ERROR_TITLE_LOADEXPENSEENTRY_FAULT:String = "LoadExpenseEntry Fault:";
		public static const ERROR_TITLE_UNEDITABLE_ENTRY:String = "Uneditable Entry:";
		public static const ERROR_TITLE_EXPENSE_TYPE_LIST_FAULT:String = "ExpenseType Fault:";
		public static const ERROR_TITLE_USER_LITE_LIST_FAULT:String = "UserLiteList Fault:";
		public static const ERROR_TITLE_GET_CLIENT_LIST_FAULT:String = "ClientList Fault:";
		public static const ERROR_TITLE_GET_PROJECT_LIST_FAULT:String = "ProjectList Fault:";
		public static const ERROR_TITLE_GET_ACTIVITY_TYPE_FAULT:String = "ActivityType Fault:";
		public static const ERROR_TITLE_NO_API_RESPONSE_DATA:String = "API Response:";
		public static const ERROR_TITLE_INCORRECT_TIME_FORMAT:String = "Invalid Format:";

		public static const ERROR_MSG_APICALL_FAULT:String = "Please check your internet connection and then try again.";
		public static const ERROR_MSG_NO_CONNECTION:String = "Please check your internet connection and then try again.";
		public static const ERROR_MSG_INVALID_USER_INFO:String = "Invalid Username or Password, please enter again!";
		public static const ERROR_MSG_INVALID_INPUT_FORMAT:String = "Please enter valid values to proceed."; 
		public static const ERROR_MSG_GET_REGISTERED_WITH_FIRM:String = "Please get registered with a firm first.";
		public static const ERROR_MSG_DATABASE_EXCEPTION:String = "Unsuccessful database operation.";
		public static const ERROR_MSG_SESSION_EXPIRED:String = "Your session has expired. Please login again.";
		public static const ERROR_MSG_UNEDITABLE_ENTRY:String = "This entry is not editable.";
		public static const ERROR_MSG_UNAVAILABLE_SERVICES:String = "Services are not available. Please try again later.";
		public static const ERROR_MSG_UNAVAILABLE_CONNECTION_AND_DATA:String = "You are not connected to the internet and no local data is available. Please try again later.";
		public static const ERROR_MSG_NO_API_RESPONSE_DATA:String = "No data available in response to API call. Please try again later.";
		public static const ERROR_MSG_INCORRECT_TIME_FORMAT:String = "Please enter valid time format e.g. (0 or 0. or 0: or 0:0 or 0.0)";
		public static const ERROR_MSG_INCORRECT_TIME_FORMAT2:String = "Please enter valid time format e.g. (01:00 or 12:59 or 1.0 or 12.9)";
		public static const ERROR_MSG_RATE_DATA_UNAVAILABLE:String = "Rate and Rate Source data unavailable and there is no internet connection. Please try again later.";
		
		////////////////////////////////////////////////////////
		
		//*****************		Mobile Firm Lables Settings	***************
		public static const FIRM_LABEL_NAME_USER:String = "User";
		public static const FIRM_LABEL_NAME_CLIENT:String = "Client";
		public static const FIRM_LABEL_NAME_PROJECT:String = "Project";
		public static const FIRM_LABEL_NAME_LABOR:String = "Labor";
		////////////////////////////////////////////////////////////////////
		
		//*****************		Flags to set in Time Entry Screen for multiple jobs to perform after SessionId is set
		public static const FLAG_GET_RATE_AND_SOURCE_API:String = API_TYPE_GET_RATE_AND_SOURCE_DETAILS;
		public static const FLAG_GET_PROJECT_DETAILS_API:String = API_TYPE_GET_PROJECT_DETAILS;
		public static const FLAG_GET_PROJECT_LIST_API:String = API_TYPE_GET_PROJECT_LIST;
		public static const FLAG_MULTIPLE_API:String = "MultipleAPI";
		///////////////////////////////////////////////////////////////////////
		
		//*****************		Images Path	***************
		public static const IMAGE_PATH_TIMER_SPINNER_MOVING:String = "Images/icons/spinner_ash_bg.swf";
		public static const IMAGE_PATH_TIMER_SPINNER_STILL:String = "Images/icons/spinner_ash_bg.gif";
		////////////////////////////////////////////////////////////////////
		
		
		public static const APP_ID:String = "Widget";
	}
}