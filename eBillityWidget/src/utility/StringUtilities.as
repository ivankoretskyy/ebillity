package utility
{
	import flash.utils.getTimer;

	public class StringUtilities
	{
		public function StringUtilities()
		{
		}

		//	Some strings can have Apostrophe (') so to add such strings into DB we need to double the apostrophe character
		public static function doubleApostropheIfExists(nameStr:String):String
		{
			var nameArr:Array = nameStr.split("'");
			nameStr = nameArr.join("''");

			return nameStr;
		}

		// This will convert a string ranging 0-9 to 00-09 (double figure)
		public static function formatSingleDigitToDouble(digit:String):String
		{
			if (int(digit) < 10)
			{
				digit = "0" + String(int(digit));
			}

			return digit;
		}

		public static function incrementTimeByOneSecond(entryTimerObj:Object):void
		{
			if ((int(entryTimerObj.second) + 1) != 60)
			{
				entryTimerObj.second = formatSingleDigitToDouble(String(int(entryTimerObj.second) + 1));
			}
			else
			{
				if ((int(entryTimerObj.minute) + 1) != 60)
				{
					entryTimerObj.second = "00";
					entryTimerObj.minute = formatSingleDigitToDouble(String(int(entryTimerObj.minute) + 1));
				}
				else
				{
					entryTimerObj.second = "00";
					entryTimerObj.minute = "00";
					entryTimerObj.hour = formatSingleDigitToDouble(String(int(entryTimerObj.hour) + 1));
				}
			}
		}

		public static function calculateTime(entryTimerObj:Object):void
		{
			var timerAtStart:int = entryTimerObj && entryTimerObj.timerAtStart ? entryTimerObj.timerAtStart : 0;
			var timerAtPause:int = timerAtPause && entryTimerObj.timerAtPause ? entryTimerObj.timerAtPause : 0;
			var totalTimer:int = getTimer() - timerAtStart + timerAtPause;

			//var h:int = totalTime
			var date:Date = new Date(getTimer() - timerAtStart + timerAtPause);

			entryTimerObj.second = formatSingleDigitToDouble(String(date.seconds));
			entryTimerObj.minute = formatSingleDigitToDouble(String(date.minutes));
			entryTimerObj.hour = formatSingleDigitToDouble(String(date.hoursUTC));
		}
	}
}
