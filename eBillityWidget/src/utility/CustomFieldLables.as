package utility
{
	public class CustomFieldLables
	{
		[Bindable]
		private var _token:String = "";
		
		[Bindable]
		private var _currencyName:String;
		
		[Bindable]
		private var _userLabelName:String;
		
		[Bindable]
		private var _clientLabelName:String;
		
		[Bindable]
		private var _projectLabelName:String;
		
		[Bindable]
		private var _laborLabelName:String;
		
		private var _isFinancialInfoSet:String = "false"; 
		
		
		public function CustomFieldLables()
		{
		}
		
		

		public function get isFinancialInfoSet():String
		{
			return _isFinancialInfoSet;
		}

		public function set isFinancialInfoSet(value:String):void
		{
			_isFinancialInfoSet = value;
		}

		[Bindable]
		public function get token():String
		{
			return _token;
		}

		public function set token(value:String):void
		{
			_token = value;
		}

		[Bindable]
		public function get laborLabelName():String
		{
			return _laborLabelName;
		}

		public function set laborLabelName(value:String):void
		{
			_laborLabelName = value;
		}

		[Bindable]
		public function get projectLabelName():String
		{
			return _projectLabelName;
		}

		public function set projectLabelName(value:String):void
		{
			_projectLabelName = value;
		}

		[Bindable]
		public function get clientLabelName():String
		{
			return _clientLabelName;
		}

		public function set clientLabelName(value:String):void
		{
			_clientLabelName = value;
		}

		[Bindable]
		public function get userLabelName():String
		{
			return _userLabelName;
		}

		public function set userLabelName(value:String):void
		{
			_userLabelName = value;
		}

		[Bindable]
		public function get currencyName():String
		{
			return _currencyName;
		}

		public function set currencyName(value:String):void
		{
			_currencyName = value;
		}

	}
}