package Controller.ApplicationManager
{
	import flash.data.SQLConnection;
	import flash.data.SQLMode;
	import flash.data.SQLResult;
	import flash.data.SQLStatement;
	import flash.desktop.NativeApplication;
	import flash.events.SQLEvent;
	import flash.events.StatusEvent;
	import flash.events.TimerEvent;
	import flash.filesystem.File;
	import flash.net.URLRequest;
	import flash.utils.Timer;

	import mx.collections.ArrayCollection;

	import Controller.ViewsManager.ViewsManager;

	import Services.RequestHandlers.SaveEntryWSReqHandler;
	import Services.RequestHandlers.WSReqHandler;
	import Services.ResponseHandlers.DeleteTimeEntry_RespHandler;
	import Services.ResponseHandlers.SaveTimeEntryDetails_RespHandler;

	import Views.LoginScreen;

	import air.net.URLMonitor;

	import utility.Constants;
	import utility.CustomFieldLables;
	import utility.SessionIdUsingValidateUser;
	import utility.StringUtilities;
	import utility.TimerFields;
	import utility.TokenUsingDoLoginFirm;
	import utility.UserInfo;

	public class ApplicationManager
	{
		/////////////////	For invoking Web Service	/////////////////////////
		private var invokeWS:SaveEntryWSReqHandler;
		//private var handleSaveExpenseEntryWSResp:SaveExpenseEntryDetails_RespHandler;
		private var handleSaveTimeEntryWSResp:SaveTimeEntryDetails_RespHandler;

		private var invokeWSSimple:WSReqHandler; //Without header info
		private var handleDeleteTimeEntryWSResp:DeleteTimeEntry_RespHandler;
		///////////////////////////////////////////////////////////////////////////

		//////		Database Connection and Manipulation	//////////////////
		private var sqlConn:SQLConnection = new SQLConnection();
		private var sqlStm:SQLStatement = new SQLStatement();
		private var dbFile:File;
		private var result:SQLResult;
		//////////////////////////////////////////////////////////////////////

		///////////		Views Manager Reference	/////////////////////////////////////////
		protected var viewsManRef:ViewsManager = ViewsManager.getViewsManagerInstance();
		/////////////////////////////////////////////////////////////////////////////////

		///////////		Network Detection Variable //////////////
		private var _monitor:URLMonitor;

		//this variable is to show connection icon on top of every screen, bound in DateConnSynch component
		[Bindable]
		private var _connectionAvailable:Boolean = false;
		////////////////////////////////////////////////////////

		///////////		Synchronization to API Variable	////////////////////////////////
		// if data in DB is not synchronized with API then this variable is false, it's bound in DateConnSynch component
		[Bindable]
		private var _isSynchronized:Boolean = false;
		[Bindable]
		private var _isUnsynchToSynching:Boolean = false;
		////////////////////////////////////////////////////////////////////////////////


		/////////	This is to implement Singleton Pattern //////////
		private static var appManRef:ApplicationManager;

		/////////	ValidateUser API response is being saved here //////////////
		private var _userLoginObject:UserInfo;
		private var _firmObjectCollection:ArrayCollection;

		/////////	DoFullLogin API response (Custom field labels) is being saved here ////////
		[Bindable]
		private var _customFieldLabelsObject:CustomFieldLables;

		////////	Selected Firm and User ID's are being saved	//////////////
		private var _userId_LoggedWith:String;
		private var _firmId_LoggedWith:String;
		private var _sessionId_LoggedWith:String = "";


		//////		User Name and Password is being saved for getting 
		// sessionID from ValidateUser API
		private var _userName:String;
		private var _password:String;


		// Timer related variables are in TimerFields class
		[Bindable]
		private var _timerFields:TimerFields = new TimerFields(1000);
		private var _timersElapsedTimeArrCol:ArrayCollection = new ArrayCollection();

		// Periodic Timer for synchronization
		private var _periodicSynchTimer:Timer = new Timer(1800000);
		private var _isSynchInProgress:Boolean = false;




		public function ApplicationManager()
		{
			Log.info("Version: " + Constants.EBILLITY_VERSION + ' ' + NativeApplication.nativeApplication.applicationDescriptor.*::versionNumber[0]);

			//*************		Network Detection **********************
			var urlRequest:URLRequest = new URLRequest(Constants.NETWORK_DETECTION_POINT);

			//urlRequest.method = "HEADER";
			monitor = new URLMonitor(urlRequest);
			monitor.pollInterval = 3000;

			monitor.addEventListener(StatusEvent.STATUS, announceNetworkStatus);
			monitor.start();
			///////////////////////////////////////////////////////////////


			//************		Connecting to Database ****************				
			dbFile = File.applicationStorageDirectory.resolvePath(Constants.DATABASE_NAME);

			sqlConn.addEventListener(SQLEvent.OPEN, dbOpenResultHandler);
			sqlConn.open(dbFile, SQLMode.CREATE);
			///////////////////////////////////////////////////////

			// setting the event listner for Timer tick
			timerFields.addEventListener(TimerEvent.TIMER, timerFields_timerDelayCompleteEventHandler);

			// Setting the event listner for Periodic Timer for synchronization
			periodicSynchTimer.addEventListener(TimerEvent.TIMER, periodicSynchTimer_timerDelayCompleteEventHandler);

		}

		public function periodicSynchTimer_timerDelayCompleteEventHandler(event:TimerEvent):void
		{
			if (isSynchInProgress == false)
			{
				synchronizeDBWithInternet();
			}
		}



		public function timerFields_timerDelayCompleteEventHandler(event:TimerEvent):void
		{
			for each (var entryTimerObj:Object in timersElapsedTimeArrCol)
			{
				if (entryTimerObj.isPaused == false)
				{
					//StringUtilities.incrementTimeByOneSecond(entryTimerObj);
					StringUtilities.calculateTime(entryTimerObj);
				}
			}

			// If entry is just selected (timerFields.justSelected == true) which means 
			// this entry is new and not in running position then do not show it's timer as 'running' in Timer GUI
			// If it is false then this means this entry is already running so show this entry timer in Timer GUI
			if (timerFields.justSelected == false)
			{
				entryTimerObj = null;
				for each (entryTimerObj in timersElapsedTimeArrCol)
				{
					if (entryTimerObj.entryId == appManRef.timerFields.entryId)
					{
						appManRef.timerFields.hour = entryTimerObj.hour;
						appManRef.timerFields.minute = entryTimerObj.minute;
						appManRef.timerFields.second = entryTimerObj.second;

						break;
					}
				}

			}



		}


		/////////	Database Open Result Handler ///////////////////

		public function dbOpenResultHandler(event:SQLEvent):void
		{
			sqlStm.sqlConnection = sqlConn;
		}

		//*************		Network Detection **********************

		[Bindable]
		public function get monitor():URLMonitor
		{
			return _monitor;
		}

		public function set monitor(value:URLMonitor):void
		{
			_monitor = value;
		}

		[Bindable]
		public function get connectionAvailable():Boolean
		{
			return _connectionAvailable;
		}

		public function set connectionAvailable(value:Boolean):void
		{
			_connectionAvailable = value;
		}

		[Bindable]
		public function get isSynchronized():Boolean
		{
			return _isSynchronized;
		}

		public function set isSynchronized(value:Boolean):void
		{
			_isSynchronized = value;
		}

		[Bindable]
		public function get isUnsynchToSynching():Boolean
		{
			return _isUnsynchToSynching;
		}

		public function set isUnsynchToSynching(value:Boolean):void
		{
			_isUnsynchToSynching = value;
		}

		private function announceNetworkStatus(event:StatusEvent):void
		{

			var loginScreenPageRef:LoginScreen = LoginScreen(viewsManRef.getViewReference(Constants.VIEW_TYPE_LOGIN));

			if (loginScreenPageRef != null)
			{
				loginScreenPageRef.enableComponent = true;
			}

			if (monitor.available == true)
			{
				connectionAvailable = true;

//It's not required in Lite version
//				var activeViewObjRef:Object = viewsManRef.getActiveScreenReference();

//				if( activeViewObjRef != null && (activeViewObjRef is RecentEntries) )
//				{
				//activeViewObjRef.getNewRatesToUpdateTotalCostForTimeEntries();
//				}
//				else
//				{
				synchronizeDBWithInternet();
//				}

			}
			else
			{
				connectionAvailable = false;
			}
		}

		////////////////////////////////////////////////////////////



		///////////		Getters and Setters		////////////////////

		public function get isSynchInProgress():Boolean
		{
			return _isSynchInProgress;
		}

		public function set isSynchInProgress(value:Boolean):void
		{
			_isSynchInProgress = value;
		}

		public function get periodicSynchTimer():Timer
		{
			return _periodicSynchTimer;
		}

		public function set periodicSynchTimer(value:Timer):void
		{
			_periodicSynchTimer = value;
		}

		public function get timersElapsedTimeArrCol():ArrayCollection
		{
			return _timersElapsedTimeArrCol;
		}

		public function set timersElapsedTimeArrCol(value:ArrayCollection):void
		{
			_timersElapsedTimeArrCol = value;
		}

		[Bindable]
		public function get timerFields():TimerFields
		{
			return _timerFields;
		}

		public function set timerFields(value:TimerFields):void
		{
			_timerFields = value;
		}

		public function get password():String
		{
			return _password;
		}

		public function set password(value:String):void
		{
			_password = value;
		}

		public function get userName():String
		{
			return _userName;
		}

		public function set userName(value:String):void
		{
			_userName = value;
		}

		public function get sessionId_LoggedWith():String
		{
			return _sessionId_LoggedWith;
		}

		public function set sessionId_LoggedWith(value:String):void
		{
			_sessionId_LoggedWith = value;
		}

		public function get userId_LoggedWith():String
		{
			return _userId_LoggedWith;
		}

		public function set userId_LoggedWith(value:String):void
		{
			_userId_LoggedWith = value;
		}

		public function get firmId_LoggedWith():String
		{
			return _firmId_LoggedWith;
		}

		public function set firmId_LoggedWith(value:String):void
		{
			_firmId_LoggedWith = value;
		}

		[Bindable]
		public function get customFieldLabelsObject():CustomFieldLables
		{
			return _customFieldLabelsObject;
		}

		public function set customFieldLabelsObject(value:CustomFieldLables):void
		{
			_customFieldLabelsObject = value;
		}

		public function get firmObjectCollection():ArrayCollection
		{
			return _firmObjectCollection;
		}

		public function set firmObjectCollection(value:ArrayCollection):void
		{
			_firmObjectCollection = value;
		}

		public function get userLoginObject():UserInfo
		{
			return _userLoginObject;
		}

		public function set userLoginObject(value:UserInfo):void
		{
			_userLoginObject = value;
		}

		////////////////	End of Getters and Setters	/////////////////

		public static function getApplicationManagerInstance():ApplicationManager
		{
			if (appManRef == null)
			{
				appManRef = new ApplicationManager();
				return appManRef;
			}
			else
			{
				return appManRef;
			}
		}


		public function flushOutAllDataMembers():void
		{
			userLoginObject = null;

			firmObjectCollection = null;

			customFieldLabelsObject = null;

			userId_LoggedWith = null;

			firmId_LoggedWith = null;

			sessionId_LoggedWith = null;

			userName = "";

			password = "";

		}



		////////	Synchronization Process		///////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////
		private var _entriesToSynchUp:int = 0;

		public function get entriesToSynchUp():int
		{
			return _entriesToSynchUp;
		}

		public function set entriesToSynchUp(value:int):void
		{
			if (value == _entriesToSynchUp)
				return;
			Log.info("entriesToSynchUp = " + value);
			_entriesToSynchUp = value;
		}


		public function onSynchronizeComplete():void
		{
			Log.info("SYNC: onSynchronizeComplete");
			isSynchInProgress = false;
			entriesToSynchUp = 0;
			Log.info("==================================================");
		}


		public function synchronizeDBWithInternet():void
		{
			Log.info("======================================================");
			Log.info("SYNC: Synchronize start");
			if (isSynchInProgress)
			{
				Log.warning("SYNC: Synchronization already in progress.");
				return;
			}

			// If connection is available then synchronize.
			// If there is no entry to synchronize then do nothing
			// If there is some entries then synch them according to the entry type (EE/TE)
			// For each type, check if that entry is a new entry (is_synch=2 and entry_id = EPOCH_NUM) or existing entry
			// is updated (with is_synch=0 and entry_id = old_entry_id)
			if (monitor.available == true)
			{
				Log.info("SYNC: Getting entries to sync from database.");

				sqlStm.text = "SELECT * FROM entries WHERE is_synch='0' OR is_synch='2'";

				sqlStm.execute();

				result = sqlStm.getResult();

				if (result.data != null)
				{
					Log.info("SYNC: Found something to sync");

					isSynchInProgress = true;
					entriesToSynchUp = result.data.length;
					if (sessionId_LoggedWith == "")
					{
						Log.info("SYNC: sessionId_LoggedWith = '' - exit");
						var validateUserAPI:SessionIdUsingValidateUser = new SessionIdUsingValidateUser();
						validateUserAPI.ifRequestFromAppManager = true;
						validateUserAPI.storeSessionIdToAppManager();
						onSynchronizeComplete();
						return;
					}

					jobToPerformAfterSessionIdSet();


				}
				else
				{
					Log.info("SYNC: Nothing to sync - exit.");
					onSynchronizeComplete();
				}

			}
		}

		public function jobToPerformAfterSessionIdSet():void
		{
			Log.info("STEP: jobToPerformAfterSessionIdSet");
			// Call the API according to the entry type and make a separate obeject of each response handler
			// for each API call and save the entry_id and entry_create_type=(new/old) in Response Handler Object first

			if (sessionId_LoggedWith != "")
			{
				Log.info("SYNC: sessionId_LoggedWith = " + sessionId_LoggedWith);

				if (customFieldLabelsObject.token == "")
				{
					Log.info("SYNC: customFieldLabelsObject.token: doing some stuff and - exit");

					var doLoginFirmAPI:TokenUsingDoLoginFirm = new TokenUsingDoLoginFirm();
					doLoginFirmAPI.ifRequestFromAppManager = true;
					doLoginFirmAPI.storeTokenToAppManager();

					onSynchronizeComplete();
					return;
				}

				jobToPerformAfterTokenSet();

			}
			else
			{
				Log.info("SYNC: sessionId_LoggedWith emtpy - exit.");

				onSynchronizeComplete();
			}

		}

		public function jobToPerformAfterTokenSet():void
		{
			Log.info("STEP: jobToPerformAfterTokenSet");

			if (customFieldLabelsObject.token != "")
			{
				Log.info("SYNC: customFieldLabelsObject.token = " + customFieldLabelsObject.token);

				isSynchronized = false;
				isUnsynchToSynching = true;

				for each (var entryRow:Object in result.data)
				{
					var param:Object = new Object();
					var context:Object = new Object();

					context.SessionId = sessionId_LoggedWith;

					if (entryRow.entry_type == "EE")
					{

					}
					else if (entryRow.entry_type == "TE")
					{
						invokeWS = new SaveEntryWSReqHandler();
						handleSaveTimeEntryWSResp = new SaveTimeEntryDetails_RespHandler();

						var timeEntryArgs:Object = new Object();

						if (entryRow.is_synch == 0) //this is an existing entry update
						{
							timeEntryArgs.TimeEntryId = entryRow.entry_id;
						}
						else if (entryRow.is_synch == 2) //this is a new entry
						{
							timeEntryArgs.TimeEntryId = "";

							/*handleSaveTimeEntryWSResp.isSynch = entryRow.is_synch;
							handleSaveTimeEntryWSResp.entryId = entryRow.entry_id;*/
						}

						handleSaveTimeEntryWSResp.isSynch = entryRow.is_synch;
						handleSaveTimeEntryWSResp.entryId = entryRow.entry_id;
						handleSaveTimeEntryWSResp.entryStatusId = entryRow.StatusId;


						timeEntryArgs.FirmId = firmId_LoggedWith;

						timeEntryArgs.ClientId = entryRow.client_id;

						timeEntryArgs.ProjectId = "0"; //entryRow.project_id;

						timeEntryArgs.UserId = entryRow.user_id;

						timeEntryArgs.TimeEntryDate = entryRow.entry_date;

						timeEntryArgs.FirmBillableActivityTypeId = entryRow.firm_billable_activity_type_id;

						timeEntryArgs.TimeEntryType = "false"; //entryRow.time_entry_type;

						timeEntryArgs.BillableTypeId = "0"; //entryRow.billable_type_id;

						timeEntryArgs.FromHour = entryRow.from_hour;

						timeEntryArgs.FromMinute = entryRow.from_minute;

						timeEntryArgs.ToHour = entryRow.to_hour;

						timeEntryArgs.ToMinute = entryRow.to_minute;

						timeEntryArgs.FlatFeeAmount = "0"; //entryRow.flat_fee_amount;

						timeEntryArgs.AwardAmount = "0"; //entryRow.award_amount;

						timeEntryArgs.ContingencyPercentage = "0"; //entryRow.contingency_percentage;

						timeEntryArgs.ContingencyAmount = "0"; //entryRow.contingency_amount;

						timeEntryArgs.IsOverrideTotalTime = entryRow.is_override_total_time;

						timeEntryArgs.OverrideTotalHour = entryRow.override_total_hour;

						timeEntryArgs.OverrideTotalMinute = entryRow.override_total_minute;

						timeEntryArgs.Rate = "0"; //entryRow.rate;

						timeEntryArgs.IsoverrideRate = "false"; //entryRow.is_override_rate;

						timeEntryArgs.OverrideRate = "0"; //entryRow.override_rate;

						timeEntryArgs.IsBillable = entryRow.is_billable;

						timeEntryArgs.ExcludeFromInvoice = entryRow.exclude_from_invoice;

						timeEntryArgs.UsePrepaidHours = "0"; //entryRow.use_prepaid_hours;

						timeEntryArgs.ElapsedHour = entryRow.elapsed_hour;

						timeEntryArgs.ElapsedMinute = entryRow.elapsed_minute;

						timeEntryArgs.LaborHour = entryRow.labor_hour;

						timeEntryArgs.LaborMinute = entryRow.labor_minute;

						timeEntryArgs.TravelHour = entryRow.travel_hour;

						timeEntryArgs.TravelMinute = entryRow.travel_minute;

						timeEntryArgs.TotalHour = entryRow.total_hour;

						timeEntryArgs.TotalMinute = entryRow.total_minute;

						timeEntryArgs.IsAutoGenerated = "false"; //entryRow.is_auto_generated;

						timeEntryArgs.InvoiceDescription = entryRow.invoice_description;

						//now requirement is to save invoice description in internal description
						timeEntryArgs.InternalDescription = entryRow.invoice_description;

						timeEntryArgs.OverrideTotalTimeComments = ''; //entryRow.override_total_time_comments;

						timeEntryArgs.InsertBy = userId_LoggedWith;

						timeEntryArgs.UpdatedBy = userId_LoggedWith;

						timeEntryArgs.AppId = Constants.APP_ID;

						timeEntryArgs.TimeEntrySource = "";

						timeEntryArgs.IsOffline = "false";

						timeEntryArgs.PayrollId = (entryRow.PayrollId ? entryRow.PayrollId : '0');


						param.context = context;
						param.timeEntryArgs = timeEntryArgs;

						invokeWS.invokeWebService(Constants.API_TYPE_SAVE_TIME_ENTRY_DETAILS, param, handleSaveTimeEntryWSResp.saveTimeEntry_ResultHandler, handleSaveTimeEntryWSResp.saveTimeEntry_FaultHandler);

					}
				}

					//appManRef.isSynchronized = true;
			}
			else
			{
				Log.info("SYNC: customFieldLabelsObject.token = '' - exit");
				onSynchronizeComplete();
			}

		}

		////////////////	End of Synchronization Process	////////////////////////////////////////////

		public function deleteEntryUsingAPI():void
		{
			if (monitor.available == true)
			{
				sqlStm.text = "SELECT * FROM delete_entries WHERE user_id='" + userId_LoggedWith + "' AND firm_id='" + firmId_LoggedWith + "'";

				sqlStm.execute();

				result = sqlStm.getResult();

				if (result.data != null)
				{
					for each (var entryRow:Object in result.data)
					{
						invokeWSSimple = new WSReqHandler();
						handleDeleteTimeEntryWSResp = new DeleteTimeEntry_RespHandler();
						//When response comes, delete the entry from delete_entries table
						handleDeleteTimeEntryWSResp.entryId = entryRow.entry_id;

						var param:Object = new Object();
						var context:Object = new Object();

						context.SessionId = sessionId_LoggedWith;

						var firmId:Object = new Object();
						var userId:Object = new Object();
						var entryIds:Object = new Object();

						firmId = entryRow.firm_id;
						userId = entryRow.user_id;
						entryIds = entryRow.entry_id;

						param.context = context;
						param.firmId = firmId;
						param.userId = userId;
						param.entryIds = entryIds;

						invokeWSSimple.invokeWebService(Constants.API_TYPE_DELETE_TIME_ENTRY, param, handleDeleteTimeEntryWSResp.deleteTimeEntry_ResultHandler, handleDeleteTimeEntryWSResp.deleteTimeEntry_FaultHandler);
					}

				}


			}

		}
	}
}
