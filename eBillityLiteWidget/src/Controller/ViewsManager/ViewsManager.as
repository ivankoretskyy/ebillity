package Controller.ViewsManager
{
	import Views.FirmList;
	import Views.LoginScreen;
	import Views.RecentEntries;
	import Views.SettingScreen;
	import Views.TimeEntryScreen;
	
	import mx.collections.ArrayCollection;
	import mx.core.FlexGlobals;
	import mx.core.IFlexDisplayObject;
	import mx.core.Window;
	import mx.managers.PopUpManager;
	
	import utility.Constants;
	
	public class ViewsManager
	{
		private static var viewsManRef:ViewsManager;
		protected var viewsRefCollection:ArrayCollection = new ArrayCollection();
		
		private var _nativeWindow_XCoordinate:Number = 0;
		private var _nativeWindow_YCoordinate:Number = 0;
		
		public function ViewsManager()
		{
			
		}

		///////		Getters and Setters		/////////////////////////////////////////////////////
		public function get nativeWindow_XCoordinate():Number
		{
			return _nativeWindow_XCoordinate;
		}

		public function set nativeWindow_XCoordinate(value:Number):void
		{
			_nativeWindow_XCoordinate = value;
		}
		
		public function get nativeWindow_YCoordinate():Number
		{
			return _nativeWindow_YCoordinate;
		}
		
		public function set nativeWindow_YCoordinate(value:Number):void
		{
			_nativeWindow_YCoordinate = value;
		}
		///////////////////////////////////////////////////////////////////////////////////////////

		public static function getViewsManagerInstance():ViewsManager
		{
			if(viewsManRef == null)
			{
				viewsManRef = new ViewsManager();
				return viewsManRef;
			}
			else
			{
				return viewsManRef;
			}
		}
		
		
		//////////////	Creating Screens Function ////////////////
		public function createScreen(viewName:String):Object
		{
			
			var newWindow:Object = null;
			if(viewName == Constants.VIEW_TYPE_LOGIN )
			{
				newWindow = new LoginScreen();
				newWindow.open(true);
				viewsRefCollection.addItem(newWindow);
				
				return newWindow;
			}
			else if ( viewName == Constants.VIEW_TYPE_FIRMLIST )
			{
				newWindow = new FirmList();
				newWindow.open(true);
				viewsRefCollection.addItem(newWindow);
				
				return newWindow;
			}
			else if (viewName == Constants.VIEW_TYPE_RECENTENTRIES)
			{
				newWindow = new RecentEntries();
				newWindow.open(true);
				viewsRefCollection.addItem(newWindow);
				
				return newWindow;
			}
			else if (viewName == Constants.VIEW_TYPE_TIMEENTRY)
			{
				newWindow = new TimeEntryScreen();
				newWindow.open(true);
				viewsRefCollection.addItem(newWindow);
			}
			else if (viewName == Constants.VIEW_TYPE_SETTING)
			{
				newWindow = new SettingScreen();
				newWindow.open(true);
				viewsRefCollection.addItem(newWindow);
			}
			
			return newWindow;
		}
		//////////////// End //////////////////////
		
		//**************	Deleting Screens Function ************
		public function deleteScreen(viewName:String):void
		{
			if(viewName == Constants.VIEW_TYPE_LOGIN)
			{
				var tempLoginRef:LoginScreen;
				tempLoginRef = LoginScreen(getViewReference(Constants.VIEW_TYPE_LOGIN));
				tempLoginRef.visible = false;
				//tempLoginRef.close();
				
				deleteViewFromViewsRefCollection(Constants.VIEW_TYPE_LOGIN);
			}
			else if (viewName == Constants.VIEW_TYPE_FIRMLIST)
			{
				var tempFirmRef:FirmList;
				tempFirmRef = FirmList(getViewReference(Constants.VIEW_TYPE_FIRMLIST));
				tempFirmRef.visible = false;
				
				deleteViewFromViewsRefCollection(Constants.VIEW_TYPE_FIRMLIST);
			}
			else if (viewName == Constants.VIEW_TYPE_RECENTENTRIES)
			{
				var tempREntriesRef:RecentEntries;
				tempREntriesRef = RecentEntries(getViewReference(Constants.VIEW_TYPE_RECENTENTRIES));
				tempREntriesRef.visible = false;
				
				deleteViewFromViewsRefCollection(Constants.VIEW_TYPE_RECENTENTRIES);
			}
			else if (viewName == Constants.VIEW_TYPE_TIMEENTRY)
			{
				var tempTimeEntryRef:TimeEntryScreen;
				tempTimeEntryRef = TimeEntryScreen(getViewReference(Constants.VIEW_TYPE_TIMEENTRY));
				tempTimeEntryRef.visible = false;
				
				deleteViewFromViewsRefCollection(Constants.VIEW_TYPE_TIMEENTRY);
			}
			else if (viewName == Constants.VIEW_TYPE_SETTING)
			{
				var tempSettingEntryRef:SettingScreen;
				tempSettingEntryRef = SettingScreen(getViewReference(Constants.VIEW_TYPE_SETTING));
				tempSettingEntryRef.visible = false;
				
				deleteViewFromViewsRefCollection(Constants.VIEW_TYPE_SETTING);
			}
			
		}
		
		private function deleteViewFromViewsRefCollection(viewName:String):void
		{
			for (var index:int = 0 ; index < viewsRefCollection.length ; index++)
			{
				var obj:Object = viewsRefCollection[index];
				
				if(viewName==Constants.VIEW_TYPE_LOGIN && (obj is LoginScreen))
				{
					break;
				}
				else if (viewName==Constants.VIEW_TYPE_FIRMLIST && (obj is FirmList))
				{
					break;
				}
				else if (viewName==Constants.VIEW_TYPE_RECENTENTRIES && (obj is RecentEntries))
				{
					break;
				}
				else if (viewName==Constants.VIEW_TYPE_TIMEENTRY && (obj is TimeEntryScreen))
				{
					break;
				}
				else if (viewName==Constants.VIEW_TYPE_SETTING && (obj is SettingScreen))
				{
					break;
				}
			}
			
			viewsRefCollection.removeItemAt(index);
		}		
		/////////////////End of Delete Screen Func	///////////
		
		
		public function getViewReference(viewName:String) : Object
		{
			var obj:Object = null;
			
			for each(var tempViewRef:Object in viewsRefCollection){
				if(viewName==Constants.VIEW_TYPE_LOGIN && (tempViewRef is LoginScreen))
				{
					return tempViewRef;
				}
				else if (viewName==Constants.VIEW_TYPE_FIRMLIST && (tempViewRef is FirmList))
				{
					return tempViewRef;
				}
				else if (viewName==Constants.VIEW_TYPE_RECENTENTRIES && (tempViewRef is RecentEntries))
				{
					return tempViewRef;
				}
				else if (viewName==Constants.VIEW_TYPE_TIMEENTRY && (tempViewRef is TimeEntryScreen))
				{
					return tempViewRef;
				}
				else if (viewName==Constants.VIEW_TYPE_SETTING && (tempViewRef is SettingScreen))
				{
					return tempViewRef;
				}
			}
			return obj;
		}
		
		
		
		public function getActiveScreenReference():Object
		{
			var activeWindow:Object = null;
			
			for each (var obj:Object in viewsRefCollection)
			{
				if( (obj is LoginScreen) && obj.visible == true )
				{
					return obj;
				}
				else if( (obj is FirmList) && obj.visible == true )
				{
					return obj;
				}
				else if( (obj is RecentEntries) && obj.visible == true )
				{
					return obj;
				}
				else if( (obj is TimeEntryScreen) && obj.visible == true )
				{
					return obj;
				}
				else if( (obj is SettingScreen) && obj.visible == true )
				{
					return obj;
				}
			}
			
			return activeWindow;
		}
		
		
		//***********	This function takes any screen as an object and
		// returns its equivalent view name
		// This function is used where ever we need to get SessionId by calling ValidateUser API
		public function getViewNameFromViewRef(viewRef:Object):String
		{
			var viewName:String = "";
			
			for each (var obj:Object in viewsRefCollection)
			{
				if( (obj == viewRef) && (obj is LoginScreen))
				{
					viewName = Constants.VIEW_TYPE_LOGIN;
					return viewName;
				}
				else if( (obj == viewRef) && (obj is FirmList))
				{
					viewName = Constants.VIEW_TYPE_FIRMLIST;
					return viewName;
				}
				else if( (obj == viewRef) && (obj is RecentEntries))
				{
					viewName = Constants.VIEW_TYPE_RECENTENTRIES;
					return viewName;
				}
				else if( (obj == viewRef) && (obj is TimeEntryScreen))
				{
					viewName = Constants.VIEW_TYPE_TIMEENTRY;
					return viewName;
				}
				else if( (obj == viewRef) && (obj is SettingScreen))
				{
					viewName = Constants.VIEW_TYPE_SETTING;
					return viewName;
				}
			}
			
			return viewName;
		}
		///////////////////////////////////////////////////////////////////////////////
	}
}