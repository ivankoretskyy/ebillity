package utility
{
	import flash.desktop.NativeApplication;
	import flash.events.ErrorEvent;
	import flash.events.Event;
	
	import air.update.ApplicationUpdaterUI;
	import air.update.events.UpdateEvent;

	public class AutoUpdate
	{
		public function AutoUpdate()
		{
		}

		private var appUpdater:ApplicationUpdaterUI = new ApplicationUpdaterUI();



		public function checkForUpdate(updateURL:String):void
		{
			// The code below is a hack to work around a bug in the framework so that CMD-Q still works on MacOS
			// This is a temporary fix until the framework is updated
			// See http://www.adobe.com/cfusion/webforums/forum/messageview.cfm?forumid=72&catid=670&threadid=1373568
			NativeApplication.nativeApplication.addEventListener(Event.EXITING, function(e:Event):void
			{
				var opened:Array = NativeApplication.nativeApplication.openedWindows;
				for (var i:int = 0; i < opened.length; i++)
				{
					opened[i].close();
				}
			});

			setApplicationVersion(); // Find the current version so we can show it below
			appUpdater.updateURL = updateURL; // Server-side XML file describing update
			appUpdater.isCheckForUpdateVisible = false; // We won't ask permission to check for an update
			appUpdater.addEventListener(UpdateEvent.INITIALIZED, onUpdate); // Once initialized, run onUpdate
			appUpdater.addEventListener(ErrorEvent.ERROR, onError); // If something goes wrong, run onError
			appUpdater.initialize(); // Initialize the update framework
		}

		private function onError(event:ErrorEvent):void
		{
			trace(event.toString());
		}

		// Find the current version for our Label below
		private function setApplicationVersion():void
		{
			var appXML:XML = NativeApplication.nativeApplication.applicationDescriptor;
			var ns:Namespace = appXML.namespace();
			var text:String = "Current version is " + appXML.ns::versionNumber;
		}

		private function onUpdate(event:UpdateEvent):void
		{
			appUpdater.checkNow(); // Go check for an update now
		}
	}
}

