package utility
{
	import Controller.ApplicationManager.ApplicationManager;
	import Controller.ViewsManager.ViewsManager;
	
	import Services.RequestHandlers.WSReqHandler;
	
	import Views.PopUp;
	
	import flash.display.DisplayObject;
	
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;

	public class TokenUsingDoLoginFirm
	{
		//************** For Custom Alert Box Declaration ****************
		protected var alert:PopUp = new PopUp();
		protected var viewsManRef:ViewsManager = ViewsManager.getViewsManagerInstance();
		protected var alertParent:DisplayObject;
		//************** For Custom Alert Box ****************
		
		////	Application Manager Reference	///////////////
		protected var appManRef:ApplicationManager = ApplicationManager.getApplicationManagerInstance();
		//////////////////////////////////////////////////////
		
		public var ifRequestFromAppManager:Boolean = false;
		
		////	For sending API Request
		protected var invokeWS:WSReqHandler = new WSReqHandler();
		public var wsRespXML:XML = new XML();
		////////////////////////////////////////////////////////////
		
		
		public function TokenUsingDoLoginFirm()
		{
		}
		
		public function storeTokenToAppManager():void
		{
			var param:Object = new Object();
			var context:Object = new Object();
			
			context.SessionId = appManRef.sessionId_LoggedWith;
			
			param.context = context;
			param.userId = appManRef.userId_LoggedWith;
			param.firmId = appManRef.firmId_LoggedWith;	
			param.ipAddress = '';
			param.appId = Constants.APP_ID;
			
			invokeWS.invokeWebService(Constants.API_TYPE_DOFULLLOGIN, param, 
				DoLoginFirmResultHandler, DoLoginFirmFaultHandler);											
			
			
		}
		
		
		public function DoLoginFirmResultHandler(event:ResultEvent):void
		{
			var activeScreenRef:Object = viewsManRef.getActiveScreenReference();
			
			wsRespXML = XML(event.message.body);
			
			appManRef.customFieldLabelsObject.token = wsRespXML.*::Body.*::DoLoginFirmResponse.*::DoLoginFirmResult.*::Token;
			
			if(ifRequestFromAppManager == true)
			{
				appManRef.jobToPerformAfterTokenSet();
			}
			else
			{
				activeScreenRef.jobToPerformAfterTokenSet();
			}
		
		}
		
		
		public function DoLoginFirmFaultHandler(event:FaultEvent):void
		{
			
		}
		
	}
}