package utility
{
	import flash.events.Event;
	import flash.utils.Timer;
	
	import mx.core.INavigatorContent;

	public class TimerFields extends Timer
	{
		private var _justSelected:Boolean = true;
		
		private var _timerAutoStartForNewEntry:Boolean = false;
		
		private var _entryId:String = "";
		
		[Bindable] 
		private var _hour:String = "00";
		
		[Bindable]
		private var _minute:String = "00";
		
		[Bindable]
		private var _second:String = "00";
		
		[Bindable]
		private var _playButton:Boolean = false;
		
		[Bindable]
		private var _playButtonDisabled:Boolean = true;
		
		[Bindable]
		private var _pauseButton:Boolean = false;
		
		[Bindable]
		private var _stopButton:Boolean = false;
		
		[Bindable]
		private var _stopButtonDisabled:Boolean = true;
		
		[Bindable] private var _roundingHour:int = 0;
		[Bindable] private var _roundingMinute:int = 0;
				
		[Bindable] private var _clientName:String = "";
		[Bindable] private var _projectName:String = "";
		
		
		
		public function TimerFields(interval:Number=1000, repeatCount:int=0)
		{
			super(interval, repeatCount);
		}

		//////////	Getters and Setters	/////////////////////////////////////////////////////

		public function get justSelected():Boolean
		{
			return _justSelected;
		}

		public function set justSelected(value:Boolean):void
		{
			_justSelected = value;
		}
		
		public function get timerAutoStartForNewEntry():Boolean
		{
			return _timerAutoStartForNewEntry;
		}
		
		public function set timerAutoStartForNewEntry(value:Boolean):void
		{
			_timerAutoStartForNewEntry = value;
		}

		public function get entryId():String
		{
			return _entryId;
		}
		
		public function set entryId(value:String):void
		{
			_entryId = value;
		}
		
		[Bindable]
		public function get hour():String
		{
			return _hour;
		}

		public function set hour(value:String):void
		{
			_hour = value;
		}

		[Bindable] public function get minute():String
		{
			return _minute;
		}

		public function set minute(value:String):void
		{
			_minute = value;
		}
		
		[Bindable]
		public function get second():String
		{
			return _second;
		}
		
		public function set second(value:String):void
		{
			_second = value;
		}
		
		[Bindable]
		public function get playButton():Boolean
		{
			return _playButton;
		}
		
		public function set playButton(value:Boolean):void
		{
			_playButton = value;
		}
		
		[Bindable]
		public function get playButtonDisabled():Boolean
		{
			return _playButtonDisabled;
		}
		
		public function set playButtonDisabled(value:Boolean):void
		{
			_playButtonDisabled = value;
		}
		
		[Bindable]
		public function get pauseButton():Boolean
		{
			return _pauseButton;
		}
		
		public function set pauseButton(value:Boolean):void
		{
			_pauseButton = value;
		}
		
		[Bindable]
		public function get stopButton():Boolean
		{
			return _stopButton;
		}
		
		public function set stopButton(value:Boolean):void
		{
			_stopButton = value;
		}
		
		[Bindable]
		public function get stopButtonDisabled():Boolean
		{
			return _stopButtonDisabled;
		}
		
		public function set stopButtonDisabled(value:Boolean):void
		{
			_stopButtonDisabled = value;
		}

		[Bindable] public function get roundingHour():int
		{
			return _roundingHour;
		}

		public function set roundingHour(value:int):void
		{
			_roundingHour = value;
		}

		[Bindable] public function get roundingMinute():int
		{
			return _roundingMinute;
		}

		public function set roundingMinute(value:int):void
		{
			_roundingMinute = value;
		}

		[Bindable] public function get clientName():String
		{
			return _clientName;
		}

		public function set clientName(value:String):void
		{
			_clientName = value;
		}

		[Bindable] public function get projectName():String
		{
			return _projectName;
		}

		public function set projectName(value:String):void
		{
			_projectName = value;
		}
		/////////////////////////////////////////////////////////////////////////////////////////////
		
		
		public function setRoundedHourAndMinute():void
		{
			// if any of the value set then do the rounding
			if(roundingHour != 0 || roundingMinute != 0)
			{
				// Calculating the Ceilling Rounded value	/////////////////////////
				var roundingTotalMin:int = roundingHour*60 + roundingMinute;
				var timerTotalMin:int = int(hour)*60 + int(minute);
				
				var roundingTemp:int = roundingTotalMin;
				for(var i:int=2; roundingTemp<=timerTotalMin ; i++)
				{
					roundingTemp = roundingTotalMin*i;
				}
				//////////////////////////////////////////////////////////////////////
				
				// now roundingTemp is holding the calculated Ceilling Rounded Value
				// so setting it to the Timer
				hour = String(int(roundingTemp/60));
				minute = String(roundingTemp - int(hour)*60);
				second = "00";
				///////////////////////////////////////////////////////////////////////
			}
			
		}
		
		

	}
}