package utility
{
	import Controller.ApplicationManager.ApplicationManager;
	import Controller.ViewsManager.ViewsManager;
	
	import Services.RequestHandlers.WSReqHandler;
	
	import Views.FirmList;
	import Views.LoginScreen;
	import Views.PopUp;
	
	import flash.data.EncryptedLocalStore;
	import flash.display.DisplayObject;
	import flash.utils.ByteArray;
	
	import mx.core.Window;
	import mx.managers.PopUpManager;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;

	public class SessionIdUsingValidateUser
	{
		
		//************** For Custom Alert Box Declaration ****************
		protected var alert:PopUp = new PopUp();
		protected var viewsManRef:ViewsManager = ViewsManager.getViewsManagerInstance();
		protected var alertParent:DisplayObject;
		//************** For Custom Alert Box ****************
		
		////	Application Manager Reference	///////////////
		protected var appManRef:ApplicationManager = ApplicationManager.getApplicationManagerInstance();
		//////////////////////////////////////////////////////
		
		public var ifRequestFromAppManager:Boolean = false;
		
		////	For sending API Request
		protected var invokeWS:WSReqHandler = new WSReqHandler();
		public var wsRespXML:XML = new XML();
		////////////////////////////////////////////////////////////
		
		
		public function SessionIdUsingValidateUser()
		{
		}
		
		
		
		
		public function storeSessionIdToAppManager():void
		{	
			var param:Object = new Object();
			
			param.userName = appManRef.userName;
			param.password = appManRef.password;				
			
			invokeWS.invokeWebService(Constants.API_TYPE_VALIDATEUSER, param, 
				ValidateUserResultHandler, ValidateUserFaultHandler);											

			
		}
		
		
		public function ValidateUserResultHandler(event:ResultEvent):void
		{
			var activeScreenRef:Object = viewsManRef.getActiveScreenReference();
			
			wsRespXML = XML(event.message.body);
			
			if(wsRespXML.*::Header.eBillityLoggedUserId == 0)
			{	
				//If user name or password has changed on server then
				// guide the user to LoginScreen and show "Session Expired" alert
				// Flush out all the values in Application Manager
				// Delete the View who called this API
				////////////////////////////////////////////////////////
				
				// Creating the LoginScreen 
				var loginScreenRef:LoginScreen;
				loginScreenRef = LoginScreen( viewsManRef.createScreen(Constants.VIEW_TYPE_LOGIN) );
				//////////////////////////////////////////////////////////////////////////
				
				//*******Custom Alert Code*****************				
				alert.message = Constants.ERROR_MSG_SESSION_EXPIRED;
				alert.titlestr = Constants.ERROR_TITLE_SESSION_EXPIRED;					
				alertParent = loginScreenRef
				
				PopUpManager.addPopUp(alert, alertParent, true);
				PopUpManager.centerPopUp(alert);
				//*******Custom Alert Code Ends*****************
				
				// Flushing out all the values stored in App Manager
				appManRef.flushOutAllDataMembers();
				////////////////////////////////////////////////////////////////////////////
				
				// Deleting the view which called this API for storing SessionId in AppManager
				viewsManRef.deleteScreen( viewsManRef.getViewNameFromViewRef(activeScreenRef) );
				///////////////////////////////////////////////////////////////////////////////
			}
			else
			{
				appManRef.sessionId_LoggedWith = wsRespXML.*::Body.*::ValidateUserResponse.*::ValidateUserResult.*::MobileGetUserFirmList[0].*::SessionId;
				if(ifRequestFromAppManager==true)
				{
					appManRef.jobToPerformAfterSessionIdSet();
				}
				else
				{
					activeScreenRef.jobToPerformAfterSessionIdSet();
				}
				
			}
		}
		
		public function ValidateUserFaultHandler(event:FaultEvent):void
		{
			var doNothign:String = '';
		}
		
	}
}