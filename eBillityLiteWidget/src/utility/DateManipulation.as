package utility
{
	public class DateManipulation
	{
		public function DateManipulation()
		{
		}
		
		public static function convertDateToYYYYMMDDStr(tempFieldDate:Date):String
		{
			///		Preparing Date Format to compare it with Datebase entries
			var fieldDateStr:String = "" ;
			
			fieldDateStr = tempFieldDate.getFullYear().toString();
			
			if((tempFieldDate.getMonth()+1) < 10)
			{
				fieldDateStr = fieldDateStr + "-0" + String( tempFieldDate.getMonth()+1 ) + "-";
			}
			else
			{
				fieldDateStr = fieldDateStr + "-" + String( tempFieldDate.getMonth()+1 ) + "-";
			}
			
			if(tempFieldDate.getDate() < 10)
			{
				fieldDateStr = fieldDateStr + "0" + tempFieldDate.getDate().toString();
			}
			else
			{
				fieldDateStr = fieldDateStr + tempFieldDate.getDate().toString();
			}
			
			return fieldDateStr;
			
		}
		
		
		// Function takes date String in YYYY-MM-DD formate and convert it to a Date object 
		public static function convertStringToDate(dateStr:String):Date
		{
			var convertedDate:Date = new Date();
			
			var dateArr:Array = dateStr.split("-");
			
			convertedDate.setFullYear(dateArr[0]);
			convertedDate.setMonth( int(dateArr[1])-1 );
			convertedDate.setDate(dateArr[2]);
			
			return convertedDate;
		}
		
		
		// This function converts Hours and Minutes in "00:00" format
		public static function convertHoutAndMinuteToStringFormat(hour:int, min:int):String
		{
			var timeStr:String = String(hour);
			
			if(hour<10)
			{
				timeStr = "0" + String(hour);
			}
			
			if(min<10)
			{
				timeStr = timeStr + ":0" + String(min);
			}
			else
			{
				timeStr = timeStr + ":" + String(min);
			}
			
			return timeStr;
		}
		
		
		// This function converts Total Hours and Total Minutes in "00:00" format
		public static function convertTotalHourAndMinuteToStringFormat(hour:int, min:int):String
		{
			var totalTime:String = "";
			if (min > 59)
			{
				var temp:int = 0;
				
				temp = min/60;
				
				hour = hour + temp;
				
				min = ( (min/60) - int(min/60) ) * 60; 
			}
			
			
			if(hour < 10)
			{
				totalTime = "0" + String(hour) + ":";
			}
			else
			{
				totalTime = String(hour) + ":";
			}
			
			if(min < 10)
			{
				totalTime = totalTime + "0" + String(min);
			}
			else
			{
				totalTime = totalTime + String(min);
			}
			
			return totalTime;
		}
		
		// Input: 2011-11-05 (string)
		// Output: 05 Nov 2011 (string)
		public static function convertStringToFormatedDateString1(str:String):String
		{
			var dateObj:Date = new Date(str);
			var conStr:String = dateObj.getDate().toPrecision(2) + " " ;
			
			return conStr;
		}
		
		
	}
}