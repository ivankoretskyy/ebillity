package utility
{
	public class CustomFieldLables
	{
		[Bindable]
		private var _token:String = "";
		
		[Bindable]
		private var _userLabelName:String;
		
		[Bindable]
		private var _clientLabelName:String;
		
		[Bindable]
		private var _activityLabelName:String;
		
		[Bindable]
		private var _laborLabelName:String;
		
		[Bindable]
		private var _IsPayrollForEachEntrySet:Boolean;
		
		
		public function CustomFieldLables()
		{
		}
		
		
		[Bindable]
		public function get token():String
		{
			return _token;
		}

		public function set token(value:String):void
		{
			_token = value;
		}
		
		[Bindable]
		public function get activityLabelName():String
		{
			return _activityLabelName;
		}
		
		public function set activityLabelName(value:String):void
		{
			_activityLabelName = value;
		}

		[Bindable]
		public function get laborLabelName():String
		{
			return _laborLabelName;
		}

		public function set laborLabelName(value:String):void
		{
			_laborLabelName = value;
		}

		[Bindable]
		public function get clientLabelName():String
		{
			return _clientLabelName;
		}

		public function set clientLabelName(value:String):void
		{
			_clientLabelName = value;
		}

		[Bindable]
		public function get userLabelName():String
		{
			return _userLabelName;
		}

		public function set userLabelName(value:String):void
		{
			_userLabelName = value;
		}
		
		[Bindable]
		public function get IsPayrollForEachEntrySet():Boolean
		{
			return _IsPayrollForEachEntrySet;
		}
		
		public function set IsPayrollForEachEntrySet(value:Boolean):void
		{
			_IsPayrollForEachEntrySet = value;
		}

	}
}