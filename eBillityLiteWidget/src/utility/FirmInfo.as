package utility
{
	public class FirmInfo
	{
		private var firmId:String;
		private var firmName:String;
		private var firmStatus:String;
		private var firmEndPointUrl:String;
		
		public function FirmInfo()
		{
		}
		
		//*************** Setters ***************

		public function setFirmId(str:String):void
		{
			firmId = str;
		}
		
		public function setFirmName(str:String):void
		{
			firmName = str;
		}
		
		public function setFirmStatus(str:String):void
		{
			firmStatus = str;
		}
		
		public function setFirmEndPointUrl(str:String):void
		{
			firmEndPointUrl = str;
		}
		///////////////// Setters end /////////////////////
		
		
		//****************** Getters **********************
		public function getFirmId():String
		{
			return firmId;
		}
		
		public function getFirmName():String
		{
			return firmName;
		}
		
		public function getFirmStatus():String
		{
			return firmStatus;
		}
		
		public function getFirmEndPointUrl():String
		{
			return firmEndPointUrl;
		}
		///////////////// Getters end //////////////////////
		
	}
}