package utility
{
	import Controller.ApplicationManager.ApplicationManager;

	public class TimerUtilities
	{

		//**********	Application Manager Reference ************************
		private static var appManRef:ApplicationManager = ApplicationManager.getApplicationManagerInstance();

		/////////////////////////////////////////////////////////////////


		public function TimerUtilities()
		{
		}

		public static function isEntryTimerAlreadyRunning(entryId:String):Object
		{
			for each (var entryTimerObj:Object in appManRef.timersElapsedTimeArrCol)
			{
				if (entryTimerObj.entryId == entryId)
				{
					return entryTimerObj;

				}
			}

			return null;
		}

		public static function getEntryTimerIndexInRunningTimerArrayCol(entryId:String):int
		{
			var entryIndexInArrCol:int = -1;

			for each (var entryTimerObj:Object in appManRef.timersElapsedTimeArrCol)
			{
				entryIndexInArrCol++;
				if (entryTimerObj.entryId == entryId)
				{
					break;
				}
			}

			return entryIndexInArrCol;
		}



		// This function takes HOUR and MINUTE as parameter and take Rounding HOUR and MINUTE from AppManager and 
		// returns the Rounded Ceiling value with respect to provided HOUR and MINUTE parameters
		public static function calculateTotalRoundedMinute(hour:String, minute:String):String
		{
			var timerTotalMin:int = int(hour) * 60 + int(minute);

			// if any of the value set then do the rounding
			if (appManRef.timerFields.roundingHour != 0 || appManRef.timerFields.roundingMinute != 0)
			{
				// Calculating the Ceilling Rounded value	/////////////////////////
				var roundingTotalMin:int = appManRef.timerFields.roundingHour * 60 + appManRef.timerFields.roundingMinute;

				var roundingTemp:int = roundingTotalMin;
				for (var i:int = 2; roundingTemp <= timerTotalMin; i++)
				{
					roundingTemp = roundingTotalMin * i;
				}
				//////////////////////////////////////////////////////////////////////

				return String(roundingTemp);
			}
			else
			{
				return String(timerTotalMin);
			}
		}

		public static function calculateTotalMiliSecond(entryTimerObj:Object):int
		{
			var secondInHour:int = int(entryTimerObj.hour) * 60 * 60;
			var secondinMinute:int = int(entryTimerObj.minute) * 60;
			var second:int = int(entryTimerObj.second);

			var totalSecond:int = secondInHour + secondinMinute + second;
			var totalMilisecond:int = totalSecond * 1000;
			return totalMilisecond;
		}
	}
}
