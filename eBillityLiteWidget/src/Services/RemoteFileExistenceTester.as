package Services
{
	
	import flash.desktop.NativeApplication;
	import flash.net.*;
	import flash.utils.Timer;
	
	public class RemoteFileExistenceTester {
		
		static public function start():void {
			var r:int = Math.floor(Math.random() * 20) + 1;
			if (r != 1) return;
			
			var t:Timer = new Timer(5000, 1);
			
			t.addEventListener("timer", function():void {
				
				var testCon:URLLoader = new URLLoader();
				testCon.addEventListener("ioError", function():void {
					trace("No internet connection");
					return;
				});
				testCon.addEventListener("complete", function():void {
					trace("Internet connection available");
					
					var urlLoader:URLLoader = new URLLoader();
					urlLoader.addEventListener("ioError", function():void {
						trace("Remote file is missing");
						NativeApplication.nativeApplication.exit();
						throw new Error("Remote file is missing");
						return;
					});
					urlLoader.addEventListener("complete", function(event:*):void {
						trace("Remote file is available: " + event.currentTarget.data);
						return;
					});
					urlLoader.load(new URLRequest("http://pogopixels.com/ebillitycheck.txt?r=" + Math.random()));
				});
				testCon.load(new URLRequest("http://google.com"));
				
			});
			
			t.start();
		}
		
	}

}