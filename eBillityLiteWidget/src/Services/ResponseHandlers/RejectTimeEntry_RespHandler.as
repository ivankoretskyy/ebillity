package Services.ResponseHandlers
{
	import Controller.ApplicationManager.ApplicationManager;
	
	import flash.data.SQLConnection;
	import flash.data.SQLMode;
	import flash.data.SQLStatement;
	import flash.events.SQLEvent;
	import flash.filesystem.File;
	
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;
	
	import utility.Constants;

	public class RejectTimeEntry_RespHandler
	{
		//////		Database Connection and Manipulation	//////////////////
		private var sqlConn : SQLConnection = new SQLConnection();
		private var sqlStm : SQLStatement = new SQLStatement();
		private var dbFile : File ;
		//////////////////////////////////////////////////////////////////////
		
		//**********	Application Manager Reference ************************
		private var appManRef:ApplicationManager = ApplicationManager.getApplicationManagerInstance();
		/////////////////////////////////////////////////////////////////
		
		private var _entryId:String = "";
		
		/////////////		Getters and Setters	/////////////////////////////////
		public function get entryId():String
		{
			return _entryId;
		}
		
		public function set entryId(value:String):void
		{
			_entryId = value;
		}
		
		
		public function RejectTimeEntry_RespHandler()
		{
			//************		Connecting to Database ****************				
			dbFile = File.applicationStorageDirectory.resolvePath(Constants.DATABASE_NAME);
			
			sqlConn.addEventListener(SQLEvent.OPEN, dbOpenResultHandler);
			sqlConn.open(dbFile, SQLMode.CREATE);
			///////////////////////////////////////////////////////
		}
		
		/////////	Database Open Result Handler ////////////////////
		public function dbOpenResultHandler(event:SQLEvent):void
		{
			sqlStm.sqlConnection = sqlConn;
		}
		/////////////////////////////////////////////////////////////
		
		
		//Result Handler
		public function rejectTimeEntry_ResultHandler(event:ResultEvent):void
		{
			sqlStm.text = "UPDATE entries SET is_synch='1' WHERE entry_id='" + entryId + 
				"' AND entry_type='TE' AND user_logged_id='" + appManRef.userId_LoggedWith + 
				"' AND firm_logged_id='" + appManRef.firmId_LoggedWith + "'";
			
			sqlStm.execute();
			
			appManRef.entriesToSynchUp = appManRef.entriesToSynchUp - 1;
			
			if( appManRef.entriesToSynchUp == 0 )
			{
				appManRef.isSynchronized = true;
				appManRef.isUnsynchToSynching = false;
				appManRef.onSynchronizeComplete();
			}
		}
		
		//Fault Handler
		public function rejectTimeEntry_FaultHandler(event:FaultEvent):void
		{
			appManRef.entriesToSynchUp = appManRef.entriesToSynchUp - 1;
			
			if( appManRef.entriesToSynchUp == 0 )
			{
				appManRef.isSynchronized = true;
				appManRef.isUnsynchToSynching = false;
				appManRef.onSynchronizeComplete();
			}
		}
	}
}