package Services.ResponseHandlers
{
	import Controller.ApplicationManager.ApplicationManager;
	import Controller.ViewsManager.ViewsManager;
	
	import Views.PopUp;
	import Views.TimeEntryScreen;
	
	import flash.data.SQLConnection;
	import flash.data.SQLMode;
	import flash.data.SQLStatement;
	import flash.display.DisplayObject;
	import flash.events.SQLEvent;
	import flash.filesystem.File;
	
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;
	
	import utility.Constants;

	public class GetProjectDetails_RespHandler
	{
		//************** For Custom Alert Box Declaration ****************
		protected var alert:PopUp = new PopUp();
		protected var viewsManRef:ViewsManager = ViewsManager.getViewsManagerInstance();
		private var alertParent:DisplayObject;
		//************** End of Custom Alert Box Declaration ****************
		
		//////		Database Connection and Manipulation	//////////////////
		private var sqlConn : SQLConnection = new SQLConnection();
		private var sqlStm : SQLStatement = new SQLStatement();
		private var dbFile : File;
		//////////////////////////////////////////////////////////////////////
		
		// This will have the complete response	//////////////////
		private var responseXML:XML = new XML();
		//////////////////////////////////////////////////////////
		
		//**********	Application Manager Reference ************************
		private var appManRef:ApplicationManager = ApplicationManager.getApplicationManagerInstance();
		/////////////////////////////////////////////////////////////////
		
		
		public function GetProjectDetails_RespHandler()
		{
			//************		Connecting to Database ****************				
			dbFile = File.applicationStorageDirectory.resolvePath(Constants.DATABASE_NAME);
			
			sqlConn.addEventListener(SQLEvent.OPEN, dbOpenResultHandler);
			sqlConn.open(dbFile, SQLMode.CREATE);
			///////////////////////////////////////////////////////
		}
		
		/////////	Database Open Result Handler ////////////////////
		public function dbOpenResultHandler(event:SQLEvent):void
		{
			sqlStm.sqlConnection = sqlConn;
		}
		/////////////////////////////////////////////////////////////
		
		public function getProjectDetails_ResultHandler(event:ResultEvent):void
		{
			var timeEntryPageRef:TimeEntryScreen = TimeEntryScreen( viewsManRef.getViewReference(Constants.VIEW_TYPE_TIMEENTRY) );
			
			if(timeEntryPageRef != null)
			{
				responseXML = XML(event.message.body);
				var xml:XML = responseXML.*::Body.*::GetProjectDetailsResponse.*::GetProjectDetailsResult[0];
				var projectId:String = String( xml.*::ProjectId );
				var billableTypeId:String = String( xml.*::BillableTypeID );
				var contingencyPercentage:String = String( xml.*::ContigencyPercentage );
				
				
				sqlStm.text = "DELETE FROM get_project_details WHERE user_id='" + 
					appManRef.userId_LoggedWith + "' and firm_id='" + appManRef.firmId_LoggedWith +  
					"' and project_id='" + projectId + "'";
				
				sqlStm.execute();
				
				sqlStm.text = "INSERT INTO get_project_details VALUES('" +  
					appManRef.userId_LoggedWith + "', '" + appManRef.firmId_LoggedWith + "', '" + 
					projectId + "', '" + billableTypeId + "', '" + contingencyPercentage + "')";
				
				sqlStm.execute();
				
				timeEntryPageRef.enableComponent = true;
				
				//timeEntryPageRef.setBillableTypeForRegularProjectFromDB();
				
			}
		}
		
		public function getProjectDetails_FaultHandler(event:FaultEvent):void
		{
			
		}
		
	}
}