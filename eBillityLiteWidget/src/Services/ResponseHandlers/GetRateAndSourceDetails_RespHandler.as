package Services.ResponseHandlers
{
	import Controller.ApplicationManager.ApplicationManager;
	import Controller.ViewsManager.ViewsManager;
	
	import Views.PopUp;
	import Views.RecentEntries;
	import Views.TimeEntryScreen;
	
	import flash.data.SQLConnection;
	import flash.data.SQLMode;
	import flash.data.SQLStatement;
	import flash.display.DisplayObject;
	import flash.events.SQLEvent;
	import flash.filesystem.File;
	
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;
	
	import utility.Constants;
	import utility.StringUtilities;

	public class GetRateAndSourceDetails_RespHandler
	{
		//************** For Custom Alert Box Declaration ****************
		protected var alert:PopUp = new PopUp();
		protected var viewsManRef:ViewsManager = ViewsManager.getViewsManagerInstance();
		private var alertParent:DisplayObject;
		//************** End of Custom Alert Box Declaration ****************
		
		//////		Database Connection and Manipulation	//////////////////
		private var sqlConn : SQLConnection = new SQLConnection();
		private var sqlStm : SQLStatement = new SQLStatement();
		private var dbFile : File;
		//////////////////////////////////////////////////////////////////////
		
		// This will have the complete response	//////////////////
		private var responseXML:XML = new XML();
		//////////////////////////////////////////////////////////
		
		//**********	Application Manager Reference ************************
		private var appManRef:ApplicationManager = ApplicationManager.getApplicationManagerInstance();
		/////////////////////////////////////////////////////////////////
		
		// Entry Id is set by RecentEntry Screen when updating Rate from Offline to Online
		private var _entryId:String = "";
		private var _clientId:String = "";
		private var _projectId:String = "";
		private var _activityId:String = "";
		////////////////////////////////////////////////////////////////////////////

		public function get activityId():String
		{
			return _activityId;
		}

		public function set activityId(value:String):void
		{
			_activityId = value;
		}

		public function get projectId():String
		{
			return _projectId;
		}

		public function set projectId(value:String):void
		{
			_projectId = value;
		}

		public function get clientId():String
		{
			return _clientId;
		}

		public function set clientId(value:String):void
		{
			_clientId = value;
		}

		public function get entryId():String
		{
			return _entryId;
		}

		public function set entryId(value:String):void
		{
			_entryId = value;
		}

		
		public function GetRateAndSourceDetails_RespHandler()
		{
			//************		Connecting to Database ****************				
			dbFile = File.applicationStorageDirectory.resolvePath(Constants.DATABASE_NAME);
			
			sqlConn.addEventListener(SQLEvent.OPEN, dbOpenResultHandler);
			sqlConn.open(dbFile, SQLMode.CREATE);
			///////////////////////////////////////////////////////
		}
		
		/////////	Database Open Result Handler ////////////////////
		public function dbOpenResultHandler(event:SQLEvent):void
		{
			sqlStm.sqlConnection = sqlConn;
		}
		/////////////////////////////////////////////////////////////
		
		public function getRateSource_ResultHandler(event:ResultEvent):void
		{/*ebillity lite code to run
			responseXML = XML(event.message.body);
			var xml:XML = responseXML.*::Body.*::GetRateAndSourcceDetailsResponse.*::GetRateAndSourcceDetailsResult[0];
			
			var timeEntryPageRef:TimeEntryScreen = TimeEntryScreen( viewsManRef.getViewReference(Constants.VIEW_TYPE_TIMEENTRY) );
			
			if(timeEntryPageRef != null)
			{
				sqlStm.text = "DELETE FROM get_rate_and_source_details WHERE user_id='" + 
					appManRef.userId_LoggedWith + "' and firm_id='" + appManRef.firmId_LoggedWith + 
					"' and client_id='" + timeEntryPageRef.clientDropDownList.selectedItem.client_id + 
					"' and project_id='" + timeEntryPageRef.projectDropDownList.selectedItem.project_id + 
					"' and firm_billable_activity_type_id='" + timeEntryPageRef.activityDropDownList.selectedItem.billable_activity_id 
					+ "'";
				
				sqlStm.execute();
				
				sqlStm.text = "INSERT INTO get_rate_and_source_details VALUES('" +  
					appManRef.userId_LoggedWith + "', '" + appManRef.firmId_LoggedWith + "', '" + 
					timeEntryPageRef.clientDropDownList.selectedItem.client_id + "', '" + 
					timeEntryPageRef.projectDropDownList.selectedItem.project_id + "', '" + 
					timeEntryPageRef.activityDropDownList.selectedItem.billable_activity_id + "', '" + 
					xml.*::Rate + "', '" + StringUtilities.doubleApostropheIfExists(xml.*::RateSource) + "', '" + xml.*::OverTimeRate + "')";
				
				sqlStm.execute();
				
				timeEntryPageRef.enableComponent = true;
				timeEntryPageRef.populateRateAndSourceFromDB();
			}
			
			
			var activeViewObjRef:Object = viewsManRef.getActiveScreenReference();
			
			if( activeViewObjRef != null && (activeViewObjRef is RecentEntries) )
			{
				sqlStm.text = "DELETE FROM get_rate_and_source_details WHERE user_id='" + 
					appManRef.userId_LoggedWith + "' and firm_id='" + appManRef.firmId_LoggedWith + 
					"' and client_id='" + clientId + 
					"' and project_id='" + projectId + 
					"' and firm_billable_activity_type_id='" + activityId
					+ "'";
				
				sqlStm.execute();
				
				sqlStm.text = "INSERT INTO get_rate_and_source_details VALUES('" +  
					appManRef.userId_LoggedWith + "', '" + appManRef.firmId_LoggedWith + "', '" + 
					clientId + "', '" + 
					projectId + "', '" + 
					activityId + "', '" + 
					xml.*::Rate + "', '" + StringUtilities.doubleApostropheIfExists(xml.*::RateSource) + "')";
				
				sqlStm.execute();
				
				activeViewObjRef.updateTotalCostInDBForNewRate(entryId, (xml.*::Rate) );
			}
			*/
		}
		
		public function getRateSource_FaultHandler(event:FaultEvent):void
		{
			var xml:String = "";
		}
	}
}