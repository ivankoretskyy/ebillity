package Services.ResponseHandlers
{
	import Controller.ApplicationManager.ApplicationManager;
	
	import flash.data.SQLConnection;
	import flash.data.SQLMode;
	import flash.data.SQLStatement;
	import flash.events.SQLEvent;
	import flash.filesystem.File;
	
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;
	
	import utility.Constants;

	public class DeleteTimeEntry_RespHandler
	{
		//************** For Custom Alert Box Declaration ****************
		/*protected var alert:PopUp = new PopUp();
		protected var viewsManRef:ViewsManager = ViewsManager.getViewsManagerInstance();
		protected var alertParent:DisplayObject;*/
		//************** End of Custom Alert Box Declaration ****************
		
		//////		Database Connection and Manipulation	//////////////////
		private var sqlConn : SQLConnection = new SQLConnection();
		private var sqlStm : SQLStatement = new SQLStatement();
		private var dbFile : File ;
		//////////////////////////////////////////////////////////////////////
		
		// This will have the complete response	//////////////////
		private var responseXML:XML = new XML();
		//////////////////////////////////////////////////////////
		
		//**********	Application Manager Reference ************************
		private var appManRef:ApplicationManager = ApplicationManager.getApplicationManagerInstance();
		/////////////////////////////////////////////////////////////////
		
		private var _entryId:String = "";
		
		/////////////		Getters and Setters	/////////////////////////////////
		public function get entryId():String
		{
			return _entryId;
		}
		
		public function set entryId(value:String):void
		{
			_entryId = value;
		}
		
		public function DeleteTimeEntry_RespHandler()
		{
			//************		Connecting to Database ****************				
			 dbFile = File.applicationStorageDirectory.resolvePath(Constants.DATABASE_NAME);
			 
			 sqlConn.addEventListener(SQLEvent.OPEN, dbOpenResultHandler);
			 sqlConn.open(dbFile, SQLMode.CREATE);
			 ///////////////////////////////////////////////////////
		}
			 
		 /////////	Database Open Result Handler ////////////////////
		 public function dbOpenResultHandler(event:SQLEvent):void
		 {
		 	sqlStm.sqlConnection = sqlConn;
		 }
		 /////////////////////////////////////////////////////////////
		 
		 
		 public function deleteTimeEntry_ResultHandler(event:ResultEvent):void
		 {
			 //responseXML = XML(event.message.body);
			 sqlStm.text = "DELETE FROM delete_entries WHERE user_id='" + appManRef.userId_LoggedWith 
				 + "' AND firm_id='" + appManRef.firmId_LoggedWith + "' AND entry_id='" + entryId + "'";
			 
			 sqlStm.execute();
		 }
		 
		 public function deleteTimeEntry_FaultHandler(event:FaultEvent):void
		 {
			 
		 }
	}
}