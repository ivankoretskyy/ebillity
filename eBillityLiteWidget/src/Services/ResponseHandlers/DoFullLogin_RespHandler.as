package Services.ResponseHandlers
{
	import Controller.ApplicationManager.ApplicationManager;
	import Controller.ViewsManager.ViewsManager;
	
	import Views.FirmList;
	import Views.PopUp;
	
	import flash.data.SQLConnection;
	import flash.data.SQLMode;
	import flash.data.SQLStatement;
	import flash.display.DisplayObject;
	import flash.events.SQLEvent;
	import flash.filesystem.File;
	
	import mx.managers.PopUpManager;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;
	
	import utility.Constants;
	import utility.CustomFieldLables;

	public class DoFullLogin_RespHandler
	{
		//************** For Custom Alert Box Declaration ****************
		protected var alert:PopUp = new PopUp();
		protected var viewsManRef:ViewsManager = ViewsManager.getViewsManagerInstance();
		protected var parent:DisplayObject;
		//************** End of Custom Alert Box Declaration ****************
		
		///////////		Database Handling variables	////////////////
		private var sqlConn : SQLConnection = new SQLConnection;
		private var sqlStm : SQLStatement = new SQLStatement;
		private var dbFile : File;
		/////////////////////////////////////////////////
		
		
		private var wsRespXML:XML = new XML();
		private var appManRef:ApplicationManager = ApplicationManager.getApplicationManagerInstance();
		
		private var customFieldLabelObj:CustomFieldLables = new CustomFieldLables();
		
		
		public function DoFullLogin_RespHandler()
		{
			customFieldLabelObj.userLabelName = "User";
			customFieldLabelObj.clientLabelName = "Client";
			customFieldLabelObj.activityLabelName = "Billable Activity";
			customFieldLabelObj.laborLabelName = "Labor";
			
			
			//************		Connecting to Database ****************				
			dbFile = File.applicationStorageDirectory.resolvePath(Constants.DATABASE_NAME);
			
			sqlConn.addEventListener(SQLEvent.OPEN, dbOpenResultHandler);
			sqlConn.open(dbFile, SQLMode.CREATE);
			///////////////////////////////////////////////////////
			
		}
		
		/////////	Database Open Result Handler ////////////////////
		public function dbOpenResultHandler(event:SQLEvent):void
		{
			sqlStm.sqlConnection = sqlConn;
		}
		/////////////////////////////////////////////////////////////
		
//***************************** DoFullLogin API material *******************************
//****************************************************************************** 
		
		////////////////	API Result Handler	///////////
		public function doFullLogin_ResultHandler(event:ResultEvent):void
		{
			// Synchronization process is being called in saveCustomFieldLabelsToAppManager()
			
			///////////	Preparing XML object
			wsRespXML = XML(event.message.body);
			
			//////////	Parsing XML and Saving Custom Field Labels to App Manager data member
			saveCustomFieldLabelsToAppManager();
			////////////////////////////////////////////////////////////////////////////
			
			/////////	Parsing XML response and Saving it to Database	//////////////
			saveCustomFieldLabelsToDatabase();
			//////////////////////////////////////////////////////////////////////////
			
			viewsManRef.createScreen(Constants.VIEW_TYPE_RECENTENTRIES);
			
			//////////	Client requirment is :
			///	If there is only one firm then FirmList Page should not be created and 
			//	the user should be taken to the RecentEntries page.
			//	So If there is only one firm then Firm Page is not created yet and 
			//	then we need to delete LoginPage instead of FirmList page.
			if(appManRef.firmObjectCollection.length == 1)
			{
				viewsManRef.deleteScreen(Constants.VIEW_TYPE_LOGIN);
			}
			else if (appManRef.firmObjectCollection.length > 1)
			{
				viewsManRef.deleteScreen(Constants.VIEW_TYPE_FIRMLIST);
			}
			
		}
		
		
		///////////////		API Fault Handler	/////////////
		public function doFullLogin_FaultHandler(event:FaultEvent):void
		{
			/////	Enable Date Grid of Firm List Page again so that user can double click
			var firmListPageRef:FirmList = 
				FirmList( viewsManRef.getViewReference(Constants.VIEW_TYPE_FIRMLIST) );
			
			firmListPageRef.firmList_dg.enabled = true;
			
			//*******Custom Alert Box Code*****************				
			alert.message = Constants.ERROR_MSG_APICALL_FAULT; 				
			alert.titlestr = Constants.ERROR_TITLE_DOFULLLOGIN_FAULT;
			parent = DisplayObject( viewsManRef.getViewReference(Constants.VIEW_TYPE_FIRMLIST) );
			
			PopUpManager.addPopUp(alert, parent, true);
			PopUpManager.centerPopUp(alert);
			//******* End of Custom Alert Box Code*****************
		}
		
		
		/////////////////	Utillity Functions	/////////////////
		public function saveCustomFieldLabelsToAppManager():void
		{
			customFieldLabelObj.token = wsRespXML.*::Body.*::DoLoginFirmResponse.*::DoLoginFirmResult.*::Token;
			customFieldLabelObj.IsPayrollForEachEntrySet = ( String(wsRespXML.*::Body.*::DoLoginFirmResponse.*::DoLoginFirmResult.*::IsPayrollForEachEntry).toLowerCase()=="false"?false:true );
			
			var firmLabelsXML:XML;
			for each ( firmLabelsXML in wsRespXML.*::Body.*::DoLoginFirmResponse.*::DoLoginFirmResult.*::FirmLabelsSettings.*::MobileFirmLabelsSettings)
			{
				if(firmLabelsXML.*::LabelName == Constants.FIRM_LABEL_NAME_USER)
				{
					customFieldLabelObj.userLabelName = firmLabelsXML.*::FirmValue;//Now client says that it should not be "LabelValue" 12-Aug-2011
				}
				else if (firmLabelsXML.*::LabelName == Constants.FIRM_LABEL_NAME_CLIENT)
				{
					customFieldLabelObj.clientLabelName = firmLabelsXML.*::FirmValue;
				}
				else if (firmLabelsXML.*::LabelName == Constants.FIRM_LABEL_NAME_ACTIVITY)
				{
					customFieldLabelObj.activityLabelName = firmLabelsXML.*::FirmValue;
				}
				else if (firmLabelsXML.*::LabelName == Constants.FIRM_LABEL_NAME_LABOR)
				{
					customFieldLabelObj.laborLabelName = firmLabelsXML.*::FirmValue;
				}
			}
						
			appManRef.customFieldLabelsObject = customFieldLabelObj;
			
			
			// Synchronization:1) When user launches the application and gets logged in with any firm
			// Synchronization() is checking itself whether connection is available or not; whether SessionId 
			// is avilable or not; whether Token is available or not.
			appManRef.synchronizeDBWithInternet();
			
		}
		
		
		private function saveCustomFieldLabelsToDatabase():void
		{
			var token:String = wsRespXML.*::Body.*::DoLoginFirmResponse.*::DoLoginFirmResult.*::Token;
			var IsPayrollForEachEntrySet:String = String(wsRespXML.*::Body.*::DoLoginFirmResponse.*::DoLoginFirmResult.*::IsPayrollForEachEntry).toLowerCase();
			
			var userLabel:String = "User";
			var clientLabel:String = "Client";
			var activityLabel:String = "Billable Activity";
			var laborLabel:String = "Labor";
			
			var firmLabelsXML:XML;
			
			for each ( firmLabelsXML in wsRespXML.*::Body.*::DoLoginFirmResponse.*::DoLoginFirmResult.*::FirmLabelsSettings.*::MobileFirmLabelsSettings)
			{
				if(firmLabelsXML.*::LabelName == Constants.FIRM_LABEL_NAME_USER)
				{
					userLabel = firmLabelsXML.*::LabelValue;
				}
				else if (firmLabelsXML.*::LabelName == Constants.FIRM_LABEL_NAME_CLIENT)
				{
					clientLabel = firmLabelsXML.*::LabelValue;
				}
				else if (firmLabelsXML.*::LabelName == Constants.FIRM_LABEL_NAME_ACTIVITY)
				{
					activityLabel = firmLabelsXML.*::FirmValue;
				}
				else if (firmLabelsXML.*::LabelName == Constants.FIRM_LABEL_NAME_LABOR)
				{
					laborLabel = firmLabelsXML.*::LabelValue;
				}
			}
			
			
			sqlStm.text = "DELETE FROM do_login_firm WHERE user_id='" + appManRef.userId_LoggedWith + 
				"' and firm_id='" + appManRef.firmId_LoggedWith + "'";
			sqlStm.execute();
			
			sqlStm.text = "INSERT INTO do_login_firm VALUES('" + appManRef.userId_LoggedWith + 
				"', '" + appManRef.firmId_LoggedWith + "', '" + token + "', '" + userLabel + 
				"', '" + clientLabel + "', '" + laborLabel + "', '" + activityLabel + "', '"+ IsPayrollForEachEntrySet +"')";
			sqlStm.execute();
			
		}
		
		
////////////////////////// End of DoFullLogin API material //////////////
	}
}