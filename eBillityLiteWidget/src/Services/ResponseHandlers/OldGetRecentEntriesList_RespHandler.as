package Services.ResponseHandlers
{
	import Controller.ViewsManager.ViewsManager;
	
	import Views.PopUp;
	import Views.RecentEntries;
	
	import flash.display.DisplayObject;
	
	import mx.collections.ArrayCollection;
	import mx.managers.PopUpManager;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;
	
	import utility.Constants;
	
	public class OldGetRecentEntriesList_RespHandler
	{
		//************** For Custom Alert Box Declaration ****************
		protected var alert:PopUp = new PopUp();
		protected var viewsManRef:ViewsManager = ViewsManager.getViewsManagerInstance();
		protected var parent:DisplayObject;
		//************** End of Custom Alert Box Declaration ****************
		
		private var responseXML:XML = new XML();
		private var recentEntriesArrCol:ArrayCollection = new ArrayCollection();
		
		
		public function GetRecentEntriesList_RespHandler()
		{
			
		}
		
		//***************************** GetRecentEntriesList API material *******************************
		//****************************************************************************** 
		
		////////////////	API Result Handler	///////////
		public function getRecentEntriesList_ResultHandler(event:ResultEvent):void
		{
			var recentEntriesPageRef:RecentEntries = RecentEntries(viewsManRef.getViewReference(Constants.VIEW_TYPE_RECENTENTRIES));;
			
			responseXML = XML(event.message.body);
			
			if(responseXML.*::Body.*::GetRecentEntriesListResponse.*::GetRecentEntriesListResult.*::RecentEntriesCount == 0)
			{
				
			}
			else
			{
				var xml:XML;
				var hour:int = 0;
				var min:int = 0;
				var totalTime:String = "";
				
				for each (xml in responseXML.*::Body.*::GetRecentEntriesListResponse.*::GetRecentEntriesListResult.*::RecentEntriesList.*::MobileRecentEntry)
				{
					var obj:Object = new Object();
					
					obj.clientField = xml.*::Client;
					obj.projectField = xml.*::Project;
					//obj.amountField = ;
					
					
					///////		Changing the Time Format for "Time Entries"
					if (xml.*::RecentType == "TE")
					{
						var timeStr:String = "";
						var timeArr:Array;
						
						timeStr = xml.*::Details;
						
						timeArr = timeStr.split(' ');
						hour = hour + int(timeArr[0]);
						min = min + int(timeArr[2]);
						
						if ( int(timeArr[0]) < 10 )
						{
							timeStr = "0" + timeArr[0] + ".";
						}
						else
						{
							timeStr = timeArr[0] + ".";
						}
						
						if ( int(timeArr[2]) < 10 )
						{
							timeStr = timeStr + "0" + timeArr[2];
						}
						else
						{
							timeStr = timeStr + timeArr[2];
						}
						
						obj.timeField = timeStr;
					}
					else
					{
						obj.timeField = xml.*::Details;
					}
					
					recentEntriesArrCol.addItem(obj);
				}
				/////////	Now the time field values format is set so we can set the Array Collection
				recentEntriesPageRef.RecentEntriesArrCol = recentEntriesArrCol;
				
				///		Now setting the total time for Recent Entry page
				/////	if minutes are greater than 59 then add the quotient to hour
				if (min > 59)
				{
					var temp:int = 0;
					
					temp = min/60;
					
					hour = hour + temp;
					
					min = ( (min/60) - int(min/60) ) * 60; 
				}
				
				
				if(hour < 10)
				{
					totalTime = "0" + String(hour) + ".";
				}
				else
				{
					totalTime = String(hour) + ".";
				}
				
				if(min < 10)
				{
					totalTime = totalTime + "0" + String(min);
				}
				else
				{
					totalTime = totalTime + String(min);
				}
				
				////	Setting the Total Time Bindable variable of RecentEntries page
				recentEntriesPageRef.totalTime = totalTime;
			}
			
			
			
			//Disable DateField component and Right Left Buttons
			recentEntriesPageRef.enableComponent = true;
			
			
		}
		
		public function getRecentEntriesList_FaultHandler(event:FaultEvent):void
		{
			//Disable DateField component and Right Left Buttons
			var recentEntriesPageRef:RecentEntries = RecentEntries(viewsManRef.getViewReference(Constants.VIEW_TYPE_RECENTENTRIES));;
			recentEntriesPageRef.enableComponent = true;
			
			//*******Custom Alert Box Code*****************				
			alert.message = event.fault.faultString; 				
			alert.titlestr = Constants.ERROR_TITLE_GETRECENTENTRIES_FAULT;
			parent = DisplayObject( viewsManRef.getViewReference(Constants.VIEW_TYPE_RECENTENTRIES) );
			
			PopUpManager.addPopUp(alert, parent, true);
			PopUpManager.centerPopUp(alert);
			//******* End of Custom Alert Box Code*****************
			
		}
	}
}