package Services.ResponseHandlers
{
	import Controller.ApplicationManager.ApplicationManager;
	import Controller.ViewsManager.ViewsManager;
	
	import Services.RequestHandlers.SaveEntryWSReqHandler;
	import Services.RequestHandlers.WSReqHandler;
	
	import Views.PopUp;
	import Views.RecentEntries;
	import Views.TimeEntryScreen;
	
	import flash.data.SQLConnection;
	import flash.data.SQLMode;
	import flash.data.SQLStatement;
	import flash.display.DisplayObject;
	import flash.events.SQLEvent;
	import flash.filesystem.File;
	
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;
	
	import utility.Constants;
	import utility.TimerUtilities;

	public class SaveTimeEntryDetails_RespHandler
	{
		//************** For Custom Alert Box Declaration ****************
		protected var alert:PopUp = new PopUp();
		protected var viewsManRef:ViewsManager = ViewsManager.getViewsManagerInstance();
		protected var alertParent:DisplayObject;
		//************** End of Custom Alert Box Declaration ****************
		
		//////		Database Connection and Manipulation	//////////////////
		private var sqlConn : SQLConnection = new SQLConnection();
		private var sqlStm : SQLStatement = new SQLStatement();
		private var dbFile : File ;
		//////////////////////////////////////////////////////////////////////
		
		// This will have the complete response	//////////////////
		private var responseXML:XML = new XML();
		//////////////////////////////////////////////////////////
		
		//**********	Application Manager Reference ************************
		private var appManRef:ApplicationManager = ApplicationManager.getApplicationManagerInstance();
		/////////////////////////////////////////////////////////////////
		
		/////////////////	For invoking Web Service	/////////////////////////
		//private var invokeWS:SaveEntryWSReqHandler; // Request handler With header info
		private var invokeWS:WSReqHandler; // Request handler With header info
		private var handleRejectTimeEntryWSResp:RejectTimeEntry_RespHandler;
		private var handleSubmitTimeEntryWSResp:SubmitTimeEntry_RespHandler;
		/////////////////////////////////////////////////////////////////////////
		
		
		private var _isSynch:String = "";
		private var _entryId:String = "";
		private var _entryStatusId:String = "";
		
		public function SaveTimeEntryDetails_RespHandler()
		{
			//************		Connecting to Database ****************				
			dbFile = File.applicationStorageDirectory.resolvePath(Constants.DATABASE_NAME);
			
			sqlConn.addEventListener(SQLEvent.OPEN, dbOpenResultHandler);
			sqlConn.open(dbFile, SQLMode.CREATE);
			///////////////////////////////////////////////////////
		}
		
		/////////	Database Open Result Handler ////////////////////
		public function dbOpenResultHandler(event:SQLEvent):void
		{
			sqlStm.sqlConnection = sqlConn;
		}
		/////////////////////////////////////////////////////////////
		
		/////////////		Getters and Setters	/////////////////////////////////
		public function get entryId():String
		{
			return _entryId;
		}
		
		public function set entryId(value:String):void
		{
			_entryId = value;
		}
		
		public function get isSynch():String
		{
			return _isSynch;
		}
		
		public function set isSynch(value:String):void
		{
			_isSynch = value;
		}
		
		public function get entryStatusId():String
		{
			return _entryStatusId;
		}
		
		public function set entryStatusId(value:String):void
		{
			_entryStatusId = value;
		}
		/////////////////////////////////////////////////////////////////////
		
		
		
		public function saveTimeEntry_ResultHandler(event:ResultEvent):void
		{
			responseXML = XML(event.message.body);
			
			var newEntryId:String = responseXML.*::Body.*::SaveTimeEntryDetailsResponse.*::SaveTimeEntryDetailsResult.*::NewIdentifier;
			
			// If a new entry is created then user comes to recent entries page and a synch call is sent as well.
			// Recent Entry page (Array Collection of DataGrid) will have EPOC number for that entry but the DB would
			// have been updated. So when user opens that entry page then EPOC number is transfered from Array Collection
			// object to entry page and page tries to load that entry (of EPOC number) from DB but the DB would have been 
			// updated by Synch function. So entry page keeps on loading for Infinite Time. So we need to update Array Collection of 
			// RecentEntry Page as well as "loadEntryId" variable of the entry page.
			// Again remember, this is all due to a new entry created.
			if( newEntryId && isSynch == "2" )
			{
				var recentEntriesPageRef:RecentEntries = RecentEntries(viewsManRef.getViewReference(Constants.VIEW_TYPE_RECENTENTRIES));
				
				if( recentEntriesPageRef != null )
				{
					for each ( var arrColObj:Object in recentEntriesPageRef.RecentEntriesArrCol )
					{
						if( arrColObj.entryId == entryId )
						{
							arrColObj.entryId = newEntryId;
						}
					}
				}
				
				var timeEntryPageRef:TimeEntryScreen = TimeEntryScreen( viewsManRef.getViewReference(Constants.VIEW_TYPE_TIMEENTRY) );
				
				if( timeEntryPageRef != null && timeEntryPageRef.loadEntryId==entryId )
				{
					timeEntryPageRef.loadEntryId = newEntryId;
				}
				
				// If this entry is still in the display timer then change its ID as well	/////////////////
				if(appManRef.timerFields.entryId == entryId)
				{
					appManRef.timerFields.entryId = newEntryId;
				}
				/////////////////////////////////////////////////////////////////////////////////////////////
				
				
				// If New entry timer was started as well then change the id in the running timers array collection as well	///////////
				var runningTimerObj:Object;
				runningTimerObj = TimerUtilities.isEntryTimerAlreadyRunning(entryId);
				
				if( runningTimerObj != null ) // This means selected entry timer is already running with EPOC number
				{
					runningTimerObj.entryId = newEntryId;
				}
				///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
				
				
			}
			////////////////////////////////////////////////////////////////////////////////////////////////////////			
			
			if(newEntryId && isSynch != "")// An Existing entry is updated
			{
				/*
				sqlStm.text = "UPDATE entries SET is_synch='1', entry_id='" + newEntryId + "' WHERE entry_id='" + entryId + 
					"' AND entry_type='TE' AND user_logged_id='" + appManRef.userId_LoggedWith + 
					"' AND firm_logged_id='" + appManRef.firmId_LoggedWith + "'";
				
				*/
				//is_synch is made to '1' in the response of Status related API when Status related API gives positive response
				// then entry is considered to be completely synched
				sqlStm.text = "UPDATE entries SET entry_id='" + newEntryId + "' WHERE entry_id='" + entryId + 
					"' AND entry_type='TE' AND user_logged_id='" + appManRef.userId_LoggedWith + 
					"' AND firm_logged_id='" + appManRef.firmId_LoggedWith + "'";
				
				sqlStm.execute();
			}
			
			
			/* This code is now shifted to new APIs: SubmitTimeENtry and RejectTimeEntry
			appManRef.entriesToSynchUp = appManRef.entriesToSynchUp - 1;
			
			if( appManRef.entriesToSynchUp == 0 )
			{
				appManRef.isSynchInProgress = false;
				appManRef.isSynchronized = true;
				appManRef.isUnsynchToSynching = false;
			}
			*/
			
			//Now sending call to SubmitTimeEntry or RejectTimeEntry depending upon Status of the entry
			var param:Object = new Object();
			var context:Object = new Object();
			
			context.SessionId = appManRef.sessionId_LoggedWith;
			
			param.context = context;
			param.entryIds = newEntryId;
			param.userId = appManRef.userId_LoggedWith;
			
			/////////////////	For invoking Web Service	/////////////////////////
			//invokeWS = new SaveEntryWSReqHandler(); // Request handler With header info
			invokeWS = new WSReqHandler(); // Request handler without header info bcz wcfstorm is calling successfully new APIs without header
			
			Log.info("entryStatusId = " + entryStatusId);
			
			if( entryStatusId == "1" )//Pending
			{
				handleRejectTimeEntryWSResp = new RejectTimeEntry_RespHandler();
				handleRejectTimeEntryWSResp.entryId = newEntryId;
				invokeWS.invokeWebService(Constants.API_TYPE_REJECT_TIME_ENTRY, param, handleRejectTimeEntryWSResp.rejectTimeEntry_ResultHandler, handleRejectTimeEntryWSResp.rejectTimeEntry_FaultHandler);
			}
			else if( entryStatusId == "2" )//Submitted
			{
				handleSubmitTimeEntryWSResp = new SubmitTimeEntry_RespHandler();
				handleSubmitTimeEntryWSResp.entryId = newEntryId;
				invokeWS.invokeWebService(Constants.API_TYPE_SUBMIT_TIME_ENTRY, param, handleSubmitTimeEntryWSResp.submitTimeEntry_ResultHandler, handleSubmitTimeEntryWSResp.submitTimeEntry_FaultHandler);
			}
			
		}
		
		public function saveTimeEntry_FaultHandler(event:FaultEvent):void
		{
			appManRef.entriesToSynchUp = appManRef.entriesToSynchUp - 1;
			
			if( appManRef.entriesToSynchUp == 0 )
			{
				appManRef.isSynchInProgress = false;
				appManRef.isSynchronized = true;
				appManRef.isUnsynchToSynching = false;
			}
		}
	}
}