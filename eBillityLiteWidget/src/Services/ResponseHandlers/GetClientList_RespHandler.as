package Services.ResponseHandlers
{
	import flash.data.SQLConnection;
	import flash.data.SQLMode;
	import flash.data.SQLStatement;
	import flash.display.DisplayObject;
	import flash.errors.SQLError;
	import flash.events.Event;
	import flash.events.SQLEvent;
	import flash.events.TimerEvent;
	import flash.filesystem.File;
	import flash.utils.Timer;

	import mx.managers.PopUpManager;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;

	import Controller.ApplicationManager.ApplicationManager;
	import Controller.ViewsManager.ViewsManager;

	import Views.PopUp;
	import Views.RecentEntries;
	import Views.SettingScreen;

	import utility.Constants;
	import utility.StringUtilities;

	public class GetClientList_RespHandler
	{
		////////////////////////////////////////////////////////////////////////

		private static var clientsHaveBeenLoaded_:Boolean = false;

		public static function get clientsHaveBeenLoaded():Boolean
		{
			return clientsHaveBeenLoaded_;
		}


		public static function postResultStuff():void
		{
			var viewsManRef:ViewsManager = ViewsManager.getViewsManagerInstance();
			var activeViewObjRef:Object = viewsManRef.getActiveScreenReference(); // viewsManRef.getViewReference(Constants.VIEW_TYPE_EXPENSEENTRY);

			if ( activeViewObjRef != null && !( activeViewObjRef is RecentEntries ) && !( activeViewObjRef is SettingScreen ))
			{
				// We do not call individual table to load in GUI but call a single function whose
				// responsibility is first to check if all 4 table data for USER and FIRM exists then 
				// load that data into GUI

				activeViewObjRef.apiCallCount = activeViewObjRef.apiCallCount - 1;

				if ( activeViewObjRef.apiCallCount == 0 )
				{
					activeViewObjRef.enableComponent = true;

					if ( activeViewObjRef.hasEntryLoadFinishedFirstTime == false )
					{
						activeViewObjRef.populateEntryPageFromDBForLoadedEntry( true );
					}
					else
					{
						activeViewObjRef.populateEntryPageFromDB();
					}

				}
			}
		}

		public function GetClientList_RespHandler()
		{
			//************		Connecting to Database ****************				
			dbFile = File.applicationStorageDirectory.resolvePath( Constants.DATABASE_NAME );

			sqlConn.addEventListener( SQLEvent.OPEN, dbOpenResultHandler );
			sqlConn.open( dbFile, SQLMode.CREATE );
			///////////////////////////////////////////////////////
		}

		//************** For Custom Alert Box Declaration ****************
		protected var alert:PopUp = new PopUp();
		protected var viewsManRef:ViewsManager = ViewsManager.getViewsManagerInstance();
		/////////////////////////////////////////////////////////////////

		// The client count. Decremented in every response handler of GetProjectList. If zero then subsequesnt code
		// for this page is called
		private var _clientCount:int = 0;

		private var _decryptDataLength:int;

		private var _decryptIndex:int;

		private var _decryptList:XMLList;

		private var _timer:Timer;
		private var alertParent:DisplayObject;
		//////////////////////////////////////////////////////////

		//**********	Application Manager Reference ************************
		private var appManRef:ApplicationManager = ApplicationManager.getApplicationManagerInstance();
		private var dbFile:File;
		//////////////////////////////////////////////////////////////////////

		// This will have the complete response	//////////////////
		private var responseXML:XML = new XML();
		//************** End of Custom Alert Box Declaration ****************

		//////		Database Connection and Manipulation	//////////////////
		private var sqlConn:SQLConnection = new SQLConnection();
		private var sqlStm:SQLStatement = new SQLStatement();

		/////////////////////////////////////////////////////////////


		public function get clientCount():int
		{
			return _clientCount;
		}

		public function set clientCount( value:int ):void
		{
			_clientCount = value;
		}

		/////////	Database Open Result Handler ////////////////////
		public function dbOpenResultHandler( event:SQLEvent ):void
		{
			sqlStm.sqlConnection = sqlConn;
		}

		public function getClientList_FaultHandler( event:FaultEvent ):void
		{
			var recentEntriesPageRef:RecentEntries = RecentEntries( viewsManRef.getViewReference( Constants.VIEW_TYPE_RECENTENTRIES ));

			var activeViewObjRef:Object = viewsManRef.getActiveScreenReference();

			if ( activeViewObjRef != null && !( activeViewObjRef is RecentEntries ) && !( activeViewObjRef is SettingScreen ))
			{
				activeViewObjRef.apiCallCount = activeViewObjRef.apiCallCount - 1;

				// This condition is applied bcz Mulitple faults may occur at time (Client, Acitivity) so muliple alerts appear
				if ( activeViewObjRef.apiCallCount == 0 )
				{
					activeViewObjRef.close();

					recentEntriesPageRef.visible = true;

					//*******Custom Alert Box Code*****************				
					alert.message = Constants.ERROR_MSG_UNAVAILABLE_SERVICES;
					alert.titlestr = Constants.ERROR_TITLE_ERROR;

					PopUpManager.addPopUp( alert, recentEntriesPageRef, true );
					PopUpManager.centerPopUp( alert );
						//******* End of Custom Alert Box Code*****************
				}
			}
		}

		public function getClientList_ResultHandler( event:ResultEvent ):void
		{
			responseXML = XML( event.message.body );

			// Delete Entries from the DB Table against User and Firm Logged in
			// Then insert the data into DB taken in response XML
			// then call the populate funtion of the Expense / Time Entry Page to populate the data from DB

			sqlStm.text = "DELETE FROM get_client_list WHERE user_id='" + appManRef.userId_LoggedWith + "' and firm_id='" + appManRef.firmId_LoggedWith + "'";
			sqlStm.execute();

			_timer = new Timer( 0, 0 );
			_timer.start();
			_timer.addEventListener( TimerEvent.TIMER, _decryptTimer );

			_decryptList = responseXML.*::Body.*::GetClientListResponse.*::GetClientListResult.*;
			_decryptDataLength = _decryptList.length() - 1;
		}

		private function _decryptData():void
		{
			var xml:XML;
			xml = _decryptList[ _decryptIndex ];

			if ( xml )
			{
				var clientNameVar:String = StringUtilities.doubleApostropheIfExists( xml.*::ClientName );
				sqlStm.text = "INSERT INTO get_client_list " + "VALUES ( " + "'" + appManRef.userId_LoggedWith + "', " + "'" + appManRef.firmId_LoggedWith + "', " + "'" + xml.*::ClientId + "', " + "'" + xml.*::CustomClientId + "', " + "'" + clientNameVar + "' )";
			}
			try
			{
				sqlStm.execute();
			}
			catch ( e:SQLError )
			{
				if ( e.detailID == 2206 )
				{
					// OK - Abort due to constraint violation.', details:'columns 'user_id', 'firm_id', 'client_id' are not unique'
					// Happens if there's duplicate user in the XML returned by the server????
					Log.warning( "Abort due to constraint violation. Duplicate client: " + sqlStm.text );
				}
				else
				{
					throw e;
				}
			}

			if ( _decryptIndex < _decryptDataLength )
			{
				_decryptIndex++;
			}
			else
			{
				_decryptIndex = 0;
				_timer.stop();
				_timer.removeEventListener( TimerEvent.TIMER, _decryptTimer );

				clientsHaveBeenLoaded_ = true;
				postResultStuff();
			}
		}

		private function _decryptTimer( e:Event ):void
		{
			var start_time:Number = new Date().time;
			do
			{
				_decryptData();
					// dont spend more than 20 ms in the decrypt timer to avoid blocking/freezing
			} while ( _timer && _timer.running && new Date().time - start_time < 1 );
		}
	}
}


