package Services.ResponseHandlers
{
	import Controller.ApplicationManager.ApplicationManager;
	import Controller.ViewsManager.ViewsManager;
	
	import Views.ExpenseEntryScreen;
	import Views.PopUp;
	import Views.RecentEntries;
	import Views.SettingScreen;
	
	import flash.data.SQLConnection;
	import flash.data.SQLMode;
	import flash.data.SQLStatement;
	import flash.display.DisplayObject;
	import flash.events.SQLEvent;
	import flash.filesystem.File;
	
	import mx.managers.PopUpManager;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;
	
	import utility.Constants;
	import utility.UserInfo;

	public class GetExpenseTypeList_RespHandler
	{
		//************** For Custom Alert Box Declaration ****************
		protected var alert:PopUp = new PopUp();
		protected var viewsManRef:ViewsManager = ViewsManager.getViewsManagerInstance();
		private var alertParent:DisplayObject;
		//************** End of Custom Alert Box Declaration ****************
		
		//////		Database Connection and Manipulation	//////////////////
		private var sqlConn : SQLConnection = new SQLConnection();
		private var sqlStm : SQLStatement = new SQLStatement();
		private var dbFile : File;
		//////////////////////////////////////////////////////////////////////
		
		// This will have the complete response	//////////////////
		private var responseXML:XML = new XML();
		//////////////////////////////////////////////////////////
		
		//**********	Application Manager Reference ************************
		private var appManRef:ApplicationManager = ApplicationManager.getApplicationManagerInstance();
		/////////////////////////////////////////////////////////////////
		
		
		public function GetExpenseTypeList_RespHandler()
		{
			//************		Connecting to Database ****************				
			dbFile = File.applicationStorageDirectory.resolvePath(Constants.DATABASE_NAME);
			
			sqlConn.addEventListener(SQLEvent.OPEN, dbOpenResultHandler);
			sqlConn.open(dbFile, SQLMode.CREATE);
			///////////////////////////////////////////////////////
		}
		
		public function getExpenseTypeList_ResultHandler(event:ResultEvent):void
		{
			responseXML = XML(event.message.body);
			
			// Delete Entries from the DB Table for the User and Firm Logged in
			// Then insert the data into DB taken in response XML
			// then call the populate funtion of the Expense / Time Entry Page to populate the data from DB
			
			sqlStm.text = "DELETE FROM get_expense_type_list WHERE user_id='" + appManRef.userId_LoggedWith + 
				"' and firm_id='" + appManRef.firmId_LoggedWith + "'";
			sqlStm.execute();
			
			
			var xml:XML ;
			
			for each ( xml in responseXML.*::Body.*::GetExpenseTypeListResponse.*::GetExpenseTypeListResult.*::MobileExpenseTypeLite )
			{
				var expTypeDesc:String = doubleApostropheIfExists( xml.*::ExpenseTypeDescription );
				
				
				sqlStm.text = "INSERT INTO get_expense_type_list " + "VALUES ( " + 
					"'" + appManRef.userId_LoggedWith + "', " + 
					"'" + appManRef.firmId_LoggedWith + "', " + 
					"'" + xml.*::ExpenseTypeId + "', " + 
					"'" + expTypeDesc + "', " + 
					"'" + xml.*::ExpenseTypeAmount + "', " + 
					"'" + xml.*::IsExpenseTypeReimbersable + "', " + 
					"'" + xml.*::IsExpenseTypeTaxable + "', " + 
					"'" + xml.*::IsExpenseTypeDefault + "', " + 
					"'" + xml.*::ExpenseTypeTaxPercentage + "' )" ;
				
				sqlStm.execute();
			}
			
			var activeViewObjRef:Object = viewsManRef.getActiveScreenReference();// viewsManRef.getViewReference(Constants.VIEW_TYPE_EXPENSEENTRY);
			
			if( activeViewObjRef != null && !(activeViewObjRef is RecentEntries) && !(activeViewObjRef is SettingScreen) )
			{
				//var expEntryPageRef:ExpenseEntryScreen = ExpenseEntryScreen ( viewReturnObj );
				
				
				//activeViewObjRef.populateExpenseEntryTypeArrColFromDB();
				// We do not call individual table to load in GUI but call a single function whose
				// responsibility is first to check if all 4 table data for USER and FIRM exists then 
				// load that data into GUI
				
				activeViewObjRef.apiCallCount = activeViewObjRef.apiCallCount - 1; 
				
				
				if( activeViewObjRef.apiCallCount == 0 )
				{
					activeViewObjRef.enableComponent = true;
					
					activeViewObjRef.populateEntryPageFromDB();
				}
				
			}
			
			
			
		}
		
		/////////	Database Open Result Handler ////////////////////
		public function dbOpenResultHandler(event:SQLEvent):void
		{
			sqlStm.sqlConnection = sqlConn;
		}
		/////////////////////////////////////////////////////////////
		
		public function getExpenseTypeList_FaultHandler(event:FaultEvent):void
		{
			var recentEntriesPageRef:RecentEntries = 
				RecentEntries( viewsManRef.getViewReference(Constants.VIEW_TYPE_RECENTENTRIES) );
			
			var activeScreenRef:Object = viewsManRef.getActiveScreenReference();
			
			if( activeScreenRef != null && !(activeScreenRef is RecentEntries) )
			{
				activeScreenRef.close();
				
				recentEntriesPageRef.visible = true;
			}
			
			//*******Custom Alert Box Code*****************				
			alert.message = Constants.ERROR_MSG_UNAVAILABLE_SERVICES; 				
			alert.titlestr = Constants.ERROR_TITLE_EXPENSE_TYPE_LIST_FAULT;
			
			PopUpManager.addPopUp(alert, recentEntriesPageRef, true);
			PopUpManager.centerPopUp(alert);
			//******* End of Custom Alert Box Code*****************
			
		}
		
		
		
		
		
		
		
		//	Some strings can have Apostrophe (') so to add such strings into DB we need to double the apostrophe character
		public function doubleApostropheIfExists(nameStr:String):String
		{
			var nameArr:Array = nameStr.split("'");
			nameStr = nameArr.join("''");
			
			return nameStr;
		}
		
		
	}
}