package Services.ResponseHandlers
{
	import Controller.ApplicationManager.ApplicationManager;
	import Controller.ViewsManager.ViewsManager;
	
	import Views.PopUp;
	import Views.RecentEntries;
	
	import flash.data.SQLConnection;
	import flash.data.SQLMode;
	import flash.data.SQLResult;
	import flash.data.SQLStatement;
	import flash.display.DisplayObject;
	import flash.events.SQLEvent;
	import flash.filesystem.File;
	
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;
	
	import utility.Constants;

	public class LoadTimeEntry_RespHandler
	{
		//	Application Manager Reference	//////////////////////////////////
		private var appManRef:ApplicationManager = ApplicationManager.getApplicationManagerInstance();
		//////////////////////////////////////////////////////////////////////
		
		///////////		Database Handling variables	////////////////
		private var sqlConn : SQLConnection = new SQLConnection;
		private var sqlStm : SQLStatement = new SQLStatement;
		private var dbFile : File;
		/////////////////////////////////////////////////
		
		//************** For Custom Alert Box Declaration ****************
		protected var alert:PopUp = new PopUp();
		protected var viewsManRef:ViewsManager = ViewsManager.getViewsManagerInstance();
		protected var alertParent:DisplayObject;
		//************** End of Custom Alert Box Declaration ****************
		
		// This will have the complete response	//////////////////
		private var responseXML:XML = new XML();
		//////////////////////////////////////////////////////////
		
		
		
		///////////		Constructor	/////////////////////////////////////////////
		public function LoadTimeEntry_RespHandler()
		{
			//************		Connecting to Database ****************				
			dbFile = File.applicationStorageDirectory.resolvePath(Constants.DATABASE_NAME);
			
			sqlConn.addEventListener(SQLEvent.OPEN, dbOpenResultHandler);
			sqlConn.open(dbFile, SQLMode.CREATE);
			///////////////////////////////////////////////////////
		}
		
		/////////	Database Open Result Handler ////////////////////
		public function dbOpenResultHandler(event:SQLEvent):void
		{
			sqlStm.sqlConnection = sqlConn;
		}
		/////////////////////////////////////////////////////////////
		
		
		
		
		public function loadTimeEntry_ResultHandler(event:ResultEvent):void
		{			
			responseXML = XML(event.message.body);
			
			var xml:XML = responseXML.*::Body.*::LoadTimeEntryResponse.*::LoadTimeEntryResult[0];
			
			
			if ( isEntrySynchronized( String(xml.*::TimeEntryId), "TE" ) )
			{
				// As entry is synchronized with DB so Update it 
				updateTimeEntryToDB();
			}
			
			//Now do the following if all Recent Entries has been updated from net to DB:
			// enableComponent = true;
			// call the function PopulateRecentEntryPageWithDB();
			
			var recentEntriesPageRef:RecentEntries = RecentEntries(viewsManRef.getViewReference(Constants.VIEW_TYPE_RECENTENTRIES));;
			
			if (recentEntriesPageRef) {
				recentEntriesPageRef.recentEntriesCount = recentEntriesPageRef.recentEntriesCount - 1;
				if( recentEntriesPageRef.recentEntriesCount == 0)
				{
					recentEntriesPageRef.enableComponent = true;
					recentEntriesPageRef.flushRecentEntriesFewVariables();
					recentEntriesPageRef.populateRecentEntriesPageFromDB();
				}
			}
			
		}
		
		public function loadTimeEntry_FaultHandler(event:FaultEvent):void
		{
			
		}
		
		
		
		//********************		Utility Functions	********************
		//******************************************************************
		
		private function isEntrySynchronized(entryId:String , entryType:String):Boolean
		{
			sqlStm.text = "SELECT entry_id, entry_type FROM entries WHERE entry_id='" + entryId + 
				"' and entry_type='" + entryType + "' and is_synch='1'";
			sqlStm.execute();
			
			var result:SQLResult = sqlStm.getResult();
			
			if( result.data == null )
			{
				return false;
			}
			else
			{
				return true;
			}
			
		}
		
		
		private function updateTimeEntryToDB():void
		{
			var xml:XML = responseXML.*::Body.*::LoadTimeEntryResponse.*::LoadTimeEntryResult[0];
			
			//	Setting entryDate for DB insertion format from YYYY-MM-DDT00:00 to YYYY-MM-DD
			var entryDate:String = xml.*::TimeEntryDate;
			var entryDateArr:Array = entryDate.split("T");
			entryDate = entryDateArr[0];
			///////////////////////////////////////////////////
			
			
			// Setting UserName for DB insertion
			var userName:String = doubleApostropheIfExists( xml.*::UserName );
			
			// Setting clientName for DB insertion
			var clientName:String = doubleApostropheIfExists( xml.*::ClientName );
			
			// Setting ProjectName for DB insertion
			var projectName:String = doubleApostropheIfExists( xml.*::ProjectName);
			
			var PayrollName:String = doubleApostropheIfExists( xml.*::PayrollName);
			
			var StatusName:String = doubleApostropheIfExists( xml.*::StatusName);
			
			
			var billableATDesc:String = doubleApostropheIfExists( xml.*::BillableActivityTypeDescription);
			
			var InvoiceDescription:String = doubleApostropheIfExists( xml.*::InvoiceDescription);
			
			var InternalDescription:String = doubleApostropheIfExists( xml.*::InternalDescription);
			
			var OverrideTotalTimeComments:String = doubleApostropheIfExists( xml.*::OverrideTotalTimeComments);
			
			var RateSource:String = doubleApostropheIfExists( xml.*::RateSource );
			
			var totalCost:String = calculateTotalCostForTimeEntry();
			
			
			sqlStm.text = "UPDATE entries SET " + 
				"user_logged_id='" + appManRef.userId_LoggedWith + "', " +
				"firm_logged_id='" + appManRef.firmId_LoggedWith + "', " + 		
				"entry_date='" + entryDate + "', " + 
				"client_id='" + xml.*::ClientId + "', " + 
				"client_name='" + clientName + "', " + 
				"project_id='" + xml.*::ProjectId + "', " + 
				"project_name='" + projectName + "', " + 
				"user_id='" + xml.*::UserId + "', " + 
				"user_name='" + userName + "', " + 
				"total_cost='" + totalCost + "', " +			
				"firm_billable_activity_type_id='" + xml.*::FirmBillableActivityTypeId + "', " + 
				"billable_activity_type_description='" + billableATDesc + "', " +  
				"time_entry_type='" + xml.*::TimeEntryType + "', " +
				"billable_type_id='" + xml.*::BillableTypeId + "', " +
				"billable_type='" + xml.*::BillableType + "', " +
				"from_hour='" + xml.*::FromHour + "', " +
				"from_minute='" + xml.*::FromMinute + "', " +
				"to_hour='" + xml.*::ToHour + "', " +
				"to_minute='" + xml.*::ToMinute + "', " +
				"flat_fee_amount='" + xml.*::FlatFeeAmount + "', " +
				"award_amount='" + xml.*::AwardAmount + "', " +
				"contingency_percentage='" + xml.*::ContingencyPercentage + "', " +
				"contingency_amount='" + xml.*::ContingencyAmount + "', " +
				"is_override_total_time='" + xml.*::IsOverrideTotalTime + "', " +
				"override_total_hour='" + xml.*::OverrideTotalHour + "', " +
				"override_total_minute='" + xml.*::OverrideTotalMinute + "', " +
				"rate='" + xml.*::Rate + "', " +
				"is_override_rate='" + xml.*::IsoverrideRate + "', " +
				"override_rate='" + xml.*::OverrideRate + "', " +
				"is_billable='" + xml.*::IsBillable + "', " +
				"exclude_from_invoice='" + xml.*::ExcludeFromInvoice + "', " +
				"use_prepaid_hours='" + xml.*::UsePrepaidHours + "', " +
				"elapsed_hour='" + xml.*::ElapsedHour + "', " +
				"elapsed_minute='" + xml.*::ElapsedMinute + "', " +
				"labor_hour='" + xml.*::LaborHour + "', " +
				"labor_minute='" + xml.*::LaborMinute + "', " +
				"travel_hour='" + xml.*::TravelHour + "', " +
				"travel_minute='" + xml.*::TravelMinute + "', " +
				"total_hour='" + xml.*::TotalHour + "', " +
				"total_minute='" + xml.*::TotalMinute + "', " +
				"is_auto_generated='" + xml.*::IsAutoGenerated + "', " +
				"invoice_description='" + InvoiceDescription + "', " +
				"internal_description='" + InternalDescription + "', " +
				"override_total_time_comments='" + OverrideTotalTimeComments + "', " +
				"is_invoiced='" + xml.*::IsInvoiced + "', " +
				"rate_source='" + RateSource + "', " +
				"PayrollId='" + xml.*::PayrollId + "', " +
				"PayrollName='" + PayrollName + "', " +
				"StatusId='" + xml.*::StatusId + "', " +
				"StatusName='" + StatusName + "' " ;
			
			if(xml.*::StatusId=="4")
			{
				sqlStm.text += ", is_editable='true' ";
			} 
			sqlStm.text +=	" WHERE entry_id='" + xml.*::TimeEntryId + "' and entry_type='TE'";
			
			
			sqlStm.execute();
		}
		
		
		//	Some strings can have Apostrophe (') so to add such strings into DB we need to double the apostrophe character  
		public function doubleApostropheIfExists(nameStr:String):String
		{
			var nameArr:Array = nameStr.split("'");
			nameStr = nameArr.join("''");
			
			return nameStr;
		}
		
		//	There is a formula to calculate the Total_Cost for Time_Entry
		private function calculateTotalCostForTimeEntry():String
		{
			var totalCost:Number = 0;
			var xml:XML = responseXML.*::Body.*::LoadTimeEntryResponse.*::LoadTimeEntryResult[0];
			
			var totalTime:Number = Number(xml.*::TotalHour) + ( Number(xml.*::TotalMinute) / 60.0 );
			//var totalHour:Number = Number(xml.*::TotalHour);
			//var totalMinute:Number =  Number(xml.*::TotalMinute) / 60.0 ;
			
			//totalTime = totalHour + totalMinute;
			//var isOverrideTime:Boolean = 
			if ( (xml.*::IsOverrideTotalTime) == "true" )
			{
				totalTime = Number(xml.*::OverrideTotalHour) + ( Number(xml.*::OverrideTotalMinute) / 60 );
			}
			
			var rate:Number = Number(xml.*::Rate);
			
			if( (xml.*::IsoverrideRate) == "true" )
			{
				rate = Number(xml.*::OverrideRate);
			}
			
			totalCost = totalTime * rate;
			//totalCost.toFixed(2);
			
			return String(totalCost);
		}
		
	}
}