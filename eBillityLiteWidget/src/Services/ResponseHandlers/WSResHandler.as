package Services.ResponseHandlers
{
	import Controller.ApplicationManager.ApplicationManager;
	import Controller.ViewsManager.ViewsManager;
	
	import Encryption.MD5;
	
	import Services.RequestHandlers.WSReqHandler;
	
	import Views.LoginScreen;
	import Views.PopUp;
	
	import flash.data.EncryptedLocalStore;
	import flash.data.SQLConnection;
	import flash.data.SQLMode;
	import flash.data.SQLResult;
	import flash.data.SQLStatement;
	import flash.display.DisplayObject;
	import flash.display.Sprite;
	import flash.events.SQLEvent;
	import flash.filesystem.File;
	import flash.utils.ByteArray;
	
	import mx.collections.ArrayCollection;
	import mx.controls.Alert;
	import mx.core.Application;
	import mx.core.FlexGlobals;
	import mx.core.WindowedApplication;
	import mx.managers.PopUpManager;
	import mx.rpc.Fault;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;
	import mx.utils.ObjectUtil;
	
	import utility.Constants;
	import utility.FirmInfo;
	import utility.UserInfo;

	public class WSResHandler
	{		
		public var wsRespXML:XML = new XML();	
		protected var appManRef:ApplicationManager = ApplicationManager.getApplicationManagerInstance();
		
		//************** For Custom Alert Box Declaration ****************
		protected var alert:PopUp = new PopUp();
		protected var viewsManRef:ViewsManager = ViewsManager.getViewsManagerInstance();
		protected var parent:DisplayObject;
		//************** For Custom Alert Box ****************
		
		///////////		Database Handling variables	////////////////
		private var sqlConn : SQLConnection = new SQLConnection;
		private var sqlStm : SQLStatement = new SQLStatement;
		private var dbFile : File;
		/////////////////////////////////////////////////
		
		
		private var invokeWS:WSReqHandler = new WSReqHandler();
		private var handleWSResp:DoFullLogin_RespHandler = new DoFullLogin_RespHandler();
		private var loginPageRef:LoginScreen;
		
		
		public function WSResHandler()
		{
			//************		Connecting to Database ****************				
			dbFile = File.applicationStorageDirectory.resolvePath(Constants.DATABASE_NAME);
			
			sqlConn.addEventListener(SQLEvent.OPEN, dbOpenResultHandler);
			sqlConn.open(dbFile, SQLMode.CREATE);
			///////////////////////////////////////////////////////
			
		}
		
		/////////	Database Open Result Handler ////////////////////
		public function dbOpenResultHandler(event:SQLEvent):void
		{
			sqlStm.sqlConnection = sqlConn;
		}
		/////////////////////////////////////////////////////////////


		
		
//***************************** Login User page material *******************************
//******************************************************************************
		//LoginScreen result handler
		public function ValidateUserResultHandler(event:ResultEvent):void
		{					
			wsRespXML = XML(event.message.body);
			loginPageRef = LoginScreen(viewsManRef.getViewReference(Constants.VIEW_TYPE_LOGIN));
		
			if(wsRespXML.*::Header.eBillityLoggedUserId == 0)
			{	
				loginPageRef.enableComponent = true;
				
				//*******Custom Alert Code*****************				
				alert.message = Constants.ERROR_MSG_INVALID_USER_INFO;
				alert.titlestr = Constants.ERROR_TITLE_INVALID_DETAILS;					
				parent = DisplayObject( viewsManRef.getViewReference(Constants.VIEW_TYPE_LOGIN) );
				
				PopUpManager.addPopUp(alert, parent, true);
				PopUpManager.centerPopUp(alert);
				//*******Custom Alert Code Ends*****************
			}
			else
			{			
				// User is authenticated so:
				// save username to ELS
				// save/update his credential in DB
				
				//Saving username to ELS
				var str:String = appManRef.userName;
				var bytes:ByteArray = new ByteArray();
				bytes.writeUTFBytes(str);
				EncryptedLocalStore.setItem("username", bytes);
				////////////////////////
				
				
				var userId:String = wsRespXML.*::Header.eBillityLoggedUserId;
				
				if(doesUserExistInDB())
				{
					//If exists then Update the DB Table
					sqlStm.text = "UPDATE user_credential SET password='" + 
						MD5.encrypt(appManRef.password) + "' , user_id='" + userId + 
						"' WHERE user_name='" + appManRef.userName + "'";
					sqlStm.execute();
				}
				else
				{
					//If doesn't exist then Insert Into the DB Table
					sqlStm.text = "INSERT INTO user_credential (user_name, password, user_id) " +
						"VALUES ('" + appManRef.userName + "', '" + MD5.encrypt(appManRef.password) + 
						"', '" + userId + "')";
					sqlStm.execute();
				}
				//////////////////////////////////////////////////////////////
				
				///////		Storing ValidateUser API response to DB Table "user_firm_list"
				updateUserFirmListDBTable(userId);
				///////////////////////////////////////////////////////////////////////////
				
				//create FirmList window if there are more than one firms
				/* if there is only one firm then take the user directly to RecentEntries
					page of that sigle firm.
				if there is no firm then prompt the user to get registered with some firm first
					and keep the user to the login page.
				*/
				var numOfFirms:int = numberOfFirms();
				if(numOfFirms == 0)						
				{
					// Promt error if no firm exists
					var loginScreeRef:LoginScreen = LoginScreen( viewsManRef.getViewReference(Constants.VIEW_TYPE_LOGIN) );
					
					loginScreeRef.enableComponent = true;
					
					//*******Custom Alert Box Code*****************				
					alert.message = Constants.ERROR_MSG_GET_REGISTERED_WITH_FIRM;  				
					alert.titlestr = Constants.ERROR_TITLE_ERROR;	
					parent = DisplayObject( loginScreeRef );
					
					PopUpManager.addPopUp(alert, parent, true);
					PopUpManager.centerPopUp(alert);
					//******* End of Custom Alert Box Code*****************		
				}
				else if (numOfFirms == 1)
				{
					saveUserInfoToAppManager();
					saveFirmInfoToAppManager();
					
					
					
					//*********		Calling DoFullLogin API if there is only one Firm
					var xml:XML = wsRespXML.*::Body.*::ValidateUserResponse.*::ValidateUserResult.*::MobileGetUserFirmList[0];
					var param:Object = new Object();	//preparing parameters for DoFullLogin API call							
					var context:Object = new Object();

					context.SessionId = String(xml.*::SessionId);
					
					param.context = context;
					param.userId = String(xml.*::UserId);
					param.firmId = String(xml.*::FirmId);
					param.ipAddress = '';
					param.appId = Constants.APP_ID;
					
					Constants.webserviceUrl = String(xml.*::EndPointUrl);
					
					/*	Call DoFullLogin (as there is only one firm now)
						and the Result Handler in DoFullLogin_ResHandler will
						guide the user to the Recent Entries page.
					*/
					invokeWS.invokeWebService(Constants.API_TYPE_DOFULLLOGIN, param, 
						handleWSResp.doFullLogin_ResultHandler, handleWSResp.doFullLogin_FaultHandler);
					
					//*****************	Call to DoFullLogin API ends	*****************
					
					
					/////	Save FirmId in App Manager firmId_LoggedWith Object for
						///// further references
					appManRef.firmId_LoggedWith = xml.*::FirmId;
					/* /////	UserId and Session_Id are being saved above 
								in saveUserInfoToAppManager(); */
					
				}
				else if (numOfFirms > 1)
				{
					saveUserInfoToAppManager();
					saveFirmInfoToAppManager();
					
					//Show firms list
					
					viewsManRef.createScreen(Constants.VIEW_TYPE_FIRMLIST);
					viewsManRef.deleteScreen(Constants.VIEW_TYPE_LOGIN);
				}
				
			}
		}
		
		//LoginScreen fault handler
		public function ValidateUserFaultHandler(event:FaultEvent):void
		{
			loginPageRef = LoginScreen(viewsManRef.getViewReference(Constants.VIEW_TYPE_LOGIN));
			loginPageRef.enableComponent = true;
			
			//*******Custom Alert Box Code*****************				
			alert.message = Constants.ERROR_MSG_APICALL_FAULT; 				
			alert.titlestr = Constants.ERROR_TITLE_VALIDATION_FAULT;	
			parent = DisplayObject( viewsManRef.getViewReference(Constants.VIEW_TYPE_LOGIN) );
			
			PopUpManager.addPopUp(alert, parent, true);
			PopUpManager.centerPopUp(alert);
			//******* End of Custom Alert Box Code*****************				
		}
		
		
		// Saving User Info from Response XML to User Object to pass it on to Application Manager
		public function saveUserInfoToAppManager():void
		{
			var userObj:UserInfo = new UserInfo();			
			var xml:XML = wsRespXML.*::Body.*::ValidateUserResponse.*::ValidateUserResult.*::MobileGetUserFirmList[0];
			
			/////////	Saving User SessionId to App Manager's sessionId_LoggedWith object 
			appManRef.sessionId_LoggedWith = xml.*::SessionId;
			
			////////	Saving UserID to App Manager's UserId_LoggedWith object
			appManRef.userId_LoggedWith = xml.*::UserId;
			
			///////// Saving from XML to User Object
			userObj.setUserId(xml.*::UserId);
			userObj.setUserName(xml.*::UserName);
			userObj.setUserType(xml.*::UserType);
			userObj.setUserStatus(xml.*::UserStatus);
			
			///////// Saving User Object to Application Manager			
			appManRef.userLoginObject = userObj;			
			
		}
		
		// Saving Firms Info from Response XML to Firm Array Collection to pass it on to Application Manager
		public function saveFirmInfoToAppManager():void
		{
			var firmObj:FirmInfo;
			var firmArrCol:ArrayCollection = new ArrayCollection();
			
			for each (var xml:XML in wsRespXML.*::Body.*::ValidateUserResponse.*::ValidateUserResult.*::MobileGetUserFirmList)
			{
				/* For testing: to check if there is only one firm
				var xml:XML = wsRespXML.*::Body.*::ValidateUserResponse.*::ValidateUserResult.*::MobileGetUserFirmList[0];
				*/
				
				firmObj = new FirmInfo();
				///////// Saving from XML to Firm Object
				firmObj.setFirmId(xml.*::FirmId);
				firmObj.setFirmName(xml.*::FirmName);
				firmObj.setFirmStatus(xml.*::FirmStatus);				
				firmObj.setFirmEndPointUrl(xml.*::EndPointUrl);				
				
				////////  Saving Firm Obj to FirmArray Collection local variable
				firmArrCol.addItem(firmObj);
			}
			
			////////  Saving FirmArray Collection reference to Application Manager variable
			appManRef.firmObjectCollection = firmArrCol;
		}
		
		public function numberOfFirms():int
		{
			var xml:XMLList = wsRespXML.*::Body.*::ValidateUserResponse.*::ValidateUserResult.*::MobileGetUserFirmList;
			
			return xml.length();
		}
		
		
		private function doesUserExistInDB():Boolean
		{
			sqlStm.text = "SELECT * FROM user_credential WHERE user_name='" + 
				appManRef.userName + "'";
			sqlStm.execute();
			
			var result:SQLResult = sqlStm.getResult();
			
			if(result.data == null)
			{
				return false;
			}
			else
			{
				return true;
			}
		}
		
		
		private function updateUserFirmListDBTable(userId:String):void
		{
			// Firs delete all record from "user_firm_list" table and then insert newly
			// received values from API response
			sqlStm.text = "DELETE FROM user_firm_list WHERE user_id='" + userId + "'";
			sqlStm.execute();
			
			for each (var xml:XML in wsRespXML.*::Body.*::ValidateUserResponse.*::ValidateUserResult.*::MobileGetUserFirmList)
			{
				var firmName:String = doubleApostropheIfExists(xml.*::FirmName);
				var userName:String = doubleApostropheIfExists(xml.*::UserName);
				var firmStatus:String = doubleApostropheIfExists(xml.*::FirmStatus);
				var userType:String = doubleApostropheIfExists(xml.*::UserType);
				var userStatus:String = doubleApostropheIfExists(xml.*::UserStatus);
				var endPointUrl:String = doubleApostropheIfExists(xml.*::EndPointUrl);
				
				sqlStm.text = "INSERT INTO user_firm_list VALUES('" + 
					xml.*::UserId + "', '" + xml.*::FirmId + "', '" + firmName + 
					"', '" + firmStatus + "', '" + userType + "', '" + 
					userStatus + "', '" + userName + "', '" + endPointUrl + "')";
				sqlStm.execute();
			}
			
		}
		
		public function doubleApostropheIfExists(nameStr:String):String
		{
			var nameArr:Array = nameStr.split("'");
			nameStr = nameArr.join("''");
			
			return nameStr;
		}
		///////////////////////////// End of Login User page details //////////////////////////////
		
		
	}
}
