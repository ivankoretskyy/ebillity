package Services.RequestHandlers
{
	import Controller.ApplicationManager.ApplicationManager;
	
	import flash.profiler.showRedrawRegions;
	import flash.utils.clearInterval;
	import flash.utils.setInterval;
	
	import mx.controls.Alert;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;
	import mx.rpc.soap.LoadEvent;
	import mx.rpc.soap.SOAPHeader;
	import mx.rpc.soap.WebService;
	
	import utility.Constants;
	
	public class SaveEntryWSReqHandler
	{
		//**********	Application Manager Reference ************************
		private var appManRef:ApplicationManager = ApplicationManager.getApplicationManagerInstance();
		/////////////////////////////////////////////////////////////////
		
		protected var wsc:WebService = null;
		public var ready:Boolean = false;
		private var lastWEBSERVICE_WSDL:String = '';
		private var lastWEBSERVICE_SERVICE:String = '';
		
		public function SaveEntryWSReqHandler()
		{
			reload();
		}
		
		private function reload():void {
			if (wsc && (Constants.WEBSERVICE_WSDL != lastWEBSERVICE_WSDL || Constants.WEBSERVICE_SERVICE != lastWEBSERVICE_SERVICE)) {
				Log.info("API URL has changed from " + lastWEBSERVICE_WSDL + " to " + Constants.WEBSERVICE_WSDL);
				wsc.removeEventListener(FaultEvent.FAULT, genericWSFault);
				wsc.removeEventListener(LoadEvent.LOAD, wsc_load);
				wsc = null;
			}
			
			if (wsc) return;
			
			Log.info("Reloading SaveEntry Web service...");
			ready = false;
			
			// Preparing Header for web service/////////////////
			var header:SOAPHeader;
			var qualifiedName:QName = new QName("eBillityTokenHeader");
			var content:String = appManRef.customFieldLabelsObject.token; //+ "||0";
			//var content:String = "178|125|5/4/2011 1:39:43 AM|0||0";
			//                      125|178|5/4/2011 7:23:20 AM|0||0
			//this is what is sent in "content"   125|178|5/4/2011 7:10:39 AM|0||0
			header = new SOAPHeader(qualifiedName,content);
			////////////////////////////////////
			
			wsc = new WebService();
			wsc.addHeader(header);
			
			wsc.wsdl = Constants.WEBSERVICE_WSDL;
			wsc.endpointURI = Constants.WEBSERVICE_SERVICE;
			wsc.useProxy = false;
			
			wsc.addEventListener(FaultEvent.FAULT, genericWSFault);
			wsc.addEventListener(LoadEvent.LOAD, wsc_load);
			
			lastWEBSERVICE_WSDL = Constants.WEBSERVICE_WSDL;
			lastWEBSERVICE_SERVICE = Constants.WEBSERVICE_SERVICE;
			
			wsc.loadWSDL();
		}
		
		protected function wsc_load(event:*):void {
			Log.info("SaveEntry WebService ready");
			ready = true;
		}
		
		public function genericWSFault(event:FaultEvent):void
		{
			trace("Connection not available while invoking WebService.");
			//Alert.show(event.message.toString());
		}
		
		public function invokeWebService(wsMethodType:String, wsParameters:Object, 
										 wsResultHandler:Function, wsFaultHandler:Function ):int
		{
			if(wsMethodType == null || wsParameters == null || wsResultHandler == null || wsFaultHandler == null)
			{
				return 0;
			}
			
			reload();
			
			var that:SaveEntryWSReqHandler = this;
			
			var intervalId:* = setInterval(function() {
				if (!that.ready) return;
				
				Log.info("invokeWebService (SaveEntry): " + wsMethodType);
				Log.dump(wsParameters);
				
				wsc.getOperation(wsMethodType).addEventListener(ResultEvent.RESULT, wsResultHandler);
				wsc.getOperation(wsMethodType).addEventListener(FaultEvent.FAULT, wsFaultHandler);
				
				wsc.getOperation(wsMethodType).arguments = wsParameters;
				wsc.getOperation(wsMethodType).send();		
				
				clearInterval(intervalId);
			}, 1);
			
			return 1;
			
		}
	}
}